({
	doInit : function(component, event) {
		var action = component.get("c.getContactUsInfo");
		
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseValue = response.getReturnValue();
				component.set("v.contactUsInfo", responseValue);
			}
		});

		$A.enqueueAction(action);
	}
})