trigger icTriggerUser on User (before insert, before update, after insert, after update) {
	icTriggerHandlerUser.IClass handler = (icTriggerHandlerUser.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerUser');

	//insert
	if(Trigger.isBefore && Trigger.isInsert) {
		handler.onBeforeInsert(Trigger.new, Trigger.newMap);
	}
/*
	if(Trigger.isAfter && Trigger.isInsert) {
		handler.onAfterInsert(Trigger.old, Trigger.new, Trigger.newMap);
	}
*/
	//update
	if(Trigger.isBefore && Trigger.isUpdate) {
		handler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
	}
/*
	if(Trigger.isAfter && Trigger.isUpdate) {
		handler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
	}
*/
}