/****************************************************************************
 * @purpose: Trigger fired on Contact Object
 * @Created Date: 19/3/16
 ***************************************************************************/
trigger ContactTrigger on Contact (before insert, before Update) {
    
    if(Trigger.isBefore){
        
        if(Trigger.isInsert){
            
            //Validation to be done before Insert of Record
            ContactTriggerHandler.isBeforeInsert(Trigger.New);            
        }
        
        if(Trigger.isUpdate){
            
            //Validation to be done before update of Record
            ContactTriggerHandler.isBeforeUpdate(Trigger.New, Trigger.oldMap);            
        }    
    }    
}