/****************************************************************************
 * @purpose: Trigger fired on Account Object
 * @Created Date: 17/3/16
 ***************************************************************************/
trigger AccountTrigger on Account (before insert, before update) {
    
    if(Trigger.isBefore){
        
        if(Trigger.isInsert){
            
            //Validation to be done before Insert of Record
            AccountTriggerHandler.isBeforeInsert(Trigger.New);            
        }
        
        if(Trigger.isUpdate){
            
            //Validation to be done before update of Record
            AccountTriggerHandler.isBeforeUpdate(Trigger.New, Trigger.oldMap);            
        }    
    }
}