/**
  * @Description : TmpLoadPerformance__c Trigger
  * @Created Date: 28 Nov 2016
 */
trigger TmpLoadPerformanceTrigger on TmpLoadPerformance__c (after insert) {
    
    
    if( Trigger.isInsert && Trigger.isAfter ) {
        
        List<TmpLoadPerformance__c> loadPerformancesToDelete = new List<TmpLoadPerformance__c>();
        for( TmpLoadPerformance__c loadPerformance : Trigger.new ) {
            loadPerformancesToDelete.add(new TmpLoadPerformance__c(id = loadPerformance.id));
        }
        
        TmpLoadPerformanceTriggerHandler.uploadRecords(Trigger.new );
        
        //--delete TmpLoadPerformance__c  records after creating opportunities, contacts and accounts
        if( loadPerformancesToDelete != null && !loadPerformancesToDelete.isEmpty() ) {
            TmpLoadPerformanceTriggerHandler.deleteLoadPerformances( loadPerformancesToDelete );
        }
    }
}