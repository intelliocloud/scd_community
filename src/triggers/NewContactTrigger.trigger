/****************************************************************************
 * @purpose: NewContactTrigger for auto populating Values in contact when any value changes in Dealer object
 * @Created Date: 09/08/16
 ***************************************************************************/
 
trigger NewContactTrigger on Contact (before insert,before update) {

Map<String,String> eng_fr=new Map<String, String>();
Schema.DescribeFieldResult fieldResult = Contact.Contact_title__c.getDescribe();
List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
for(Schema.PicklistEntry p: ple){
system.debug('==============='+p.getValue());
system.debug('==============='+p.getLabel());
/****************************************************************************
 * Login to get the locale for language change
 ***************************************************************************/
eng_fr.put(p.getValue(), p.getLabel());
}

/****************************************************************************
 * On Insert
 ***************************************************************************/
if(trigger.isinsert){
if(trigger.new[0].MailingStreet==null){
try{
Account acc=[SELECT ShippingStreet,ShippingCountry,ShippingState,ShippingPostalCode,ShippingCity FROM Account where id =:trigger.new[0].Accountid];
trigger.new[0].MailingStreet = acc.ShippingStreet;
trigger.new[0].MailingCountry= acc.ShippingCountry;
trigger.new[0].MailingState= acc.ShippingState;
trigger.new[0].MailingPostalCode = acc.ShippingPostalCode ;
trigger.new[0].MailingCity= acc.ShippingCity;
/****************************************************************************
 * If language is French
 ***************************************************************************/
if(Userinfo.getLanguage() =='fr'){
    trigger.new[0].Title= eng_fr.get(trigger.new[0].Contact_title__c);
    }
    
  /****************************************************************************
 * If language is English
 ***************************************************************************/
else{
    trigger.new[0].Title= trigger.new[0].Contact_title__c;
}
} Catch(Exception e){
}
}
}
/****************************************************************************
 * On Vlaue Update
 ***************************************************************************/
if(trigger.isupdate){
try{
Account acc=[SELECT ShippingStreet,ShippingCountry,ShippingState,ShippingPostalCode,ShippingCity,Sales_Rep_View__c,Fax,Phone FROM Account where id =:trigger.new[0].Accountid];
trigger.new[0].MailingStreet = acc.ShippingStreet;
trigger.new[0].MailingCountry= acc.ShippingCountry;
trigger.new[0].MailingState= acc.ShippingState;
trigger.new[0].MailingPostalCode = acc.ShippingPostalCode ;
trigger.new[0].MailingCity= acc.ShippingCity;
trigger.new[0].Fax= acc.Fax;
trigger.new[0].Phone = acc.Phone;
system.debug(trigger.new[0].Contact_title__c);
system.debug(eng_fr.get(trigger.new[0].Contact_title__c));
if(Userinfo.getLanguage() =='fr'){
    trigger.new[0].Title= eng_fr.get(trigger.new[0].Contact_title__c);}
else{
    trigger.new[0].Title= trigger.new[0].Contact_title__c;
    }
//system.debug('++ayush rastogi++'+ trigger.new[0].Contact_title__c);
User u=[SELECT Id FROM User where name=: acc.Sales_Rep_View__c limit 1];
trigger.new[0].OwnerId=u.id;
}
Catch(Exception e){
}
}
}