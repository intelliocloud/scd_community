public class icRepositoryUser implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		User getUserInfoById(String userId);
		List<User> getPartnerUsersByContactIds(Set<String> contactIds);
		List<Profile> getPartnerProfile();
		List<UserRole> getPartnerUserRolesByAccountIds(Set<String> accountIds);
		void createUsers(List<User> newUsers);
		void editUsers(List<User> users);
	}

	public class Impl implements IClass {

		public User getUserInfoById(String userId) {
			return [SELECT Id, FirstName, LastName, ContactId, Title, Text_Email__c, Phone, MobilePhone, SmallPhotoUrl, FullPhotoUrl FROM User WHERE Id = :userId];
		}
		
		public List<User> getPartnerUsersByContactIds(Set<String> contactIds) {
			return [SELECT Id, FirstName, LastName, ContactId FROM User WHERE ContactId = :contactIds];
		}

		public List<Profile> getPartnerProfile() {
			return [SELECT Id, Name FROM Profile WHERE UserType = 'PowerPartner'];
		}

		public List<UserRole> getPartnerUserRolesByAccountIds(Set<String> accountIds) {
			return [SELECT Id, Name, PortalAccountId FROM UserRole WHERE PortalAccountId IN :accountIds];
		}

		public void createUsers(List<User> newUsers) {
			insert newUsers;
		}

		public void editUsers(List<User> users) {
			update users;
		}
	}
}