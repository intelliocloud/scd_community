@isTest
public with sharing class icTestRepositoryAccount {

	static testMethod void test_getAccountByName() {
		icRepositoryAccount.IClass repository = (icRepositoryAccount.IClass) icObjectFactory.GetSingletonInstance('icRepositoryAccount');

		List<Account> testResult = repository.getAccountByName('AccountName');
	}

	static testMethod void test_getPartnerAccountsFromList() {
		icRepositoryAccount.IClass repository = (icRepositoryAccount.IClass) icObjectFactory.GetSingletonInstance('icRepositoryAccount');

		List<Account> testResult = repository.getPartnerAccountsFromList(new Set<String>());
	}
}