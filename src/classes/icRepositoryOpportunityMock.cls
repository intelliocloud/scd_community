@isTest
public class icRepositoryOpportunityMock implements icRepositoryOpportunity.IClass{

	public List<RecordType> getRecordTypes() {
		//Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getRecordTypes');
		//params.put('parameter', parameter);
		Return (List<RecordType>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getRecordTypes');
	}

	public void createOpportunities(List<Opportunity> newOpportunities) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'createOpportunities');
		params.put('newOpportunities', newOpportunities);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'createOpportunities');
	}
}