@isTest
public with sharing class icTestTriggerLead {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icTriggerHandlerLead', new icTriggerHandlerLeadMock());
	}

	static testMethod void test_leadTrigger() {
		initTest();

		Account testAccount = new Account(Name = 'TestAccount');
		insert testAccount;

		Lead testLead = new Lead(
			FirstName = 'FirstName'
			,LastName = 'LastName'
			,Email = 'test@fake.com'
			,Company = 'Company'
			,Dealer__c = testAccount.Id);
		insert testLead;

		update testLead;

		delete testLead;

		undelete testLead;
	}
}