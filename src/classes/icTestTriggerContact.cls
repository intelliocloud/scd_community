@isTest
public with sharing class icTestTriggerContact {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icTriggerHandlerLead', new icTriggerHandlerLeadMock());
	}

	static testMethod void test_contactTrigger() {
		initTest();

		Account testAccount = new Account(
			Name = 'Test Account');
		insert testAccount;

		Contact testContact = new Contact(
			AccountId = testAccount.Id
			,FirstName = 'FirstName'
			,LastName = 'LastName'
			,Email = 'test@fake.com');
		insert testContact;

		update testContact;

		delete testContact;

		undelete testContact;
	}
}