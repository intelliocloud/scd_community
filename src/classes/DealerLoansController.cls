/*
    @description Fetch loan records related to "DealerTrackMonthlyOpp" and "Monthly_AutoWorx_DT_Route_One" record type.
    @date 21/04/2016
    --------------------------------------------------------------------------------------------------------------------
    @Version 1.0
    @ModifiedDate: 05th April 2017. 
    @modificationPurpose: Fixed the issue if Load name contains apostropy('). 
*/

public with sharing class DealerLoansController {
    // Current Account record Id
    private Id accountId {get; set;}
    // Dealers Loan records detials in JSON format
    public String dealerLoansJSON {get; set;}
    // List of Loans records related to "Monthly_AutoWorx_DT_Route_One" record type.
    public List<String> listAWXMonthlyPerformanceOneFields {get; set;}
    // List of Loans records related to "DealerTrackMonthlyOpp" record type.
    public List<String> listDealerTrackMonthlyLoansFields {get; set;}

    /*
        @description Get current Account record Id and respective fieldsets related
                     to "Monthly_AutoWorx_DT_Route_One" and "DealerTrackMonthlyOpp" record type.
    */
    public DealerLoansController(ApexPages.StandardController controller) {
        // Get current Account record Id
        accountId = (Id) controller.getId();
        System.debug('accountId : ' + accountId);

        // Get fieldset related to "Monthly_AutoWorx_DT_Route_One" record type.
        listAWXMonthlyPerformanceOneFields = new List<String>();
        for(Schema.FieldSetMember field : SObjectType.Opportunity.FieldSets.AWXMonthlyPerformanceOne.getFields()){
            listAWXMonthlyPerformanceOneFields.add('"' + field.label + '"');
        }

        // Get fieldset related to "DealerTrackMonthlyOpp" record type.
        listDealerTrackMonthlyLoansFields = new List<String>();
        for(Schema.FieldSetMember field : SObjectType.Opportunity.FieldSets.DealerTrackMonthlyLoans.getFields()){
            listDealerTrackMonthlyLoansFields.add('"' + field.label + '"');
        }
    }

    /*
        @description Get Loan records based on record type provided by method parameter.
    */
    public PageReference getLoanRecords(String recordTypeName) {
        List<Opportunity> listDealerLoans;
        List<LoansWrapper> listLoanWrapperRecords = new List<LoansWrapper>();
        try{
            // Get RecordType record based on parameter "recordTypeName"
            RecordType dealerTrackMonthlyLoansRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName  = :recordTypeName LIMIT 1];
            System.debug('dealerTrackMonthlyLoansRecordType : ' + dealerTrackMonthlyLoansRecordType );

            listDealerLoans = new List<Opportunity>();
            // Get Loan records based on record type.
            if(dealerTrackMonthlyLoansRecordType != NULL){
                listDealerLoans = [SELECT Id, Name, First_Submits_Ratio__c, Prime_Book_to_Look__c,
                                                DESJ_Book_of_Prime__c, Amount, CloseDate,DESJ_Look_all__c, DESJ_approved_all__c,
                                                DESJ_book_all__c, Bookto_Look__c, Calc_Avg_Total_Amount_Financed__c, Asset_Type_FABD__c
                                                FROM Opportunity
                                                WHERE AccountId = :accountId
                                                AND
                                                RecordTypeId = :dealerTrackMonthlyLoansRecordType.Id];
                System.debug('listDealerLoans : ' + listDealerLoans);
                for(Opportunity recordLoan : listDealerLoans){
                    listLoanWrapperRecords.add(new LoansWrapper(recordLoan));
                }

                dealerLoansJSON = String.escapeSingleQuotes(JSON.serialize(listLoanWrapperRecords));
                System.debug('dealerLoansJSON : ' + dealerLoansJSON);
            }
        }catch(Exception e){
                System.debug('Error: ' + e.getMessage());
        }

        return null;
    }

    /*
        @description Get Loan records related to "DealerTrackMonthlyOpp" record type.
    */
    public PageReference getDealerTrackMonthlyLoans() {
        getLoanRecords('DealerTrackMonthlyOpp');
        return null;
    }

    /*
        @description Get Loan records related to "Monthly_AutoWorx_DT_Route_One" record type.
    */
    public PageReference getAWXMonthlyPerformanceOne() {
        getLoanRecords('Monthly_AutoWorx_DT_Route_One');
        return null;
    }

    public class LoansWrapper{
        public String recordId {get;set;}
        public String Name {get;set;}
        public Double First_Submits_Ratio {get;set;}
        public Double Prime_Book_to_Look {get;set;}
        public Double DESJ_Book_of_Prime {get;set;}
        public Double Amount {get;set;}
        public Datetime CloseDate {get;set;}
        public Double DESJ_Look_all {get;set;}
        public Double DESJ_approved_all {get;set;}
        public Double DESJ_book_all {get;set;}
        public Double Bookto_Look {get;set;}
        public Double Calc_Avg_Total_Amount_Financed {get;set;}
        public String Asset_Type_FABD {get;set;}

        public LoansWrapper(Opportunity recordLoan){
            recordId = recordLoan.Id;
            Name = recordLoan.Name;
            First_Submits_Ratio = Double.valueOf(recordLoan.First_Submits_Ratio__c);
            Prime_Book_to_Look = Double.valueOf( recordLoan.Prime_Book_to_Look__c);
            DESJ_Book_of_Prime = Double.valueOf(recordLoan.DESJ_Book_of_Prime__c);
            Amount = recordLoan.Amount;
            System.debug('recordLoan.CloseDate : ' + recordLoan.CloseDate);
            if(recordLoan.CloseDate != NULL)
                CloseDate = getLocalGMT(recordLoan.CloseDate);
            else
                CloseDate = recordLoan.CloseDate;
            System.debug('CloseDate : ' + CloseDate);
            DESJ_Look_all = Double.valueOf(recordLoan.DESJ_Look_all__c);
            DESJ_approved_all = Double.valueOf(recordLoan.DESJ_approved_all__c);
            DESJ_book_all = Double.valueOf(recordLoan.DESJ_book_all__c);
            Bookto_Look = Double.valueOf(recordLoan.Bookto_Look__c);
            Calc_Avg_Total_Amount_Financed = Double.valueOf(recordLoan.Calc_Avg_Total_Amount_Financed__c);
            Asset_Type_FABD = recordLoan.Asset_Type_FABD__c;
        }

        public Datetime getLocalGMT(Datetime gmtDateTime){
            Date d = gmtDateTime.dateGmt();
            Time t = gmtDateTime.timeGmt();
            return Datetime.newInstance(d,t);
        }
    }
}