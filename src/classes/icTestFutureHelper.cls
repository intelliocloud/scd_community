@isTest
public with sharing class icTestFutureHelper {

	static testMethod void test_updatePartnerUserRole() {
		Profile randomProfile = [SELECT Id, Name, UserType FROM Profile WHERE UserType = 'Standard' LIMIT 1];

		User newTestUser = new User(
			Username = 'createUsers@test.mock',	
			ProfileId = randomProfile.Id,
			Alias = 'testmock',
			Email = 'createUsers@test.mock',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'firstName',
			LastName = 'lastName',
			CommunityNickname = 'testmock',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		insert newTestUser;

		UserRole ranfomUserRole = [SELECT Id, Name FROM UserRole WHERE PortalAccountId = null LIMIT 1];

		Map<Id, Id> mapUserRoleIdByUserId = new Map<Id, Id>();
		mapUserRoleIdByUserId.put(newTestUser.Id, ranfomUserRole.Id);

		Test.startTest();
		icFutureHelper.updatePartnerUserRole(mapUserRoleIdByUserId);
		Test.stopTest();
	}
}