global with sharing class icDTOContactUsInfo {
	@AuraEnabled global String firstName {get;set;}
	@AuraEnabled global String lastName {get;set;}
	@AuraEnabled global String title {get;set;}
	@AuraEnabled global String email {get;set;}
	@AuraEnabled global String phone {get;set;}
	@AuraEnabled global String cell {get;set;}
	@AuraEnabled global String image {get;set;}
}