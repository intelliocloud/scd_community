@isTest
public with sharing class icTestRepositoryContact {

	static testMethod void test_getContactInfoById() {
		icRepositoryContact.IClass repository = (icRepositoryContact.IClass) icObjectFactory.GetSingletonInstance('icRepositoryContact');

		Contact newContact = new Contact(
			FirstName = 'first name',
			LastName = 'last name'
		);
		insert newContact;

		Contact testResult = repository.getContactInfoById(newContact.Id);
	}

	static testMethod void test_getContactsByDealerNames() {
		icRepositoryContact.IClass repository = (icRepositoryContact.IClass) icObjectFactory.GetSingletonInstance('icRepositoryContact');

		List<Contact> testResult = repository.getContactsByDealerNames(new Set<String>());
	}
}