@isTest
public with sharing class icTestRepositoryOpportunity {

	static testMethod void test_getRecordTypes() {
		icRepositoryOpportunity.IClass repository = (icRepositoryOpportunity.IClass) icObjectFactory.GetSingletonInstance('icRepositoryOpportunity');

		List<RecordType> testResult = repository.getRecordTypes();
	}

	static testMethod void test_CRUD() {
		icRepositoryOpportunity.IClass repository = (icRepositoryOpportunity.IClass) icObjectFactory.GetSingletonInstance('icRepositoryOpportunity');

		Opportunity newOpportunity = new Opportunity(Name = 'Opp Name', StageName = 'Book', CloseDate = Date.today());

		repository.createOpportunities(new List<Opportunity> {newOpportunity});		
	}
}