public class icRepositoryMetadata implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Lead_Conversion_Settings__mdt> getAutoConvertLeadSettings();
		List<Configuration_Label__mdt> getConfigurationLabelsByType(String configurationType);
		List<Partner_User_Configuration__mdt> getPartnerUserConfigurations();
	}

	public class Impl implements IClass {

		public List<Lead_Conversion_Settings__mdt> getAutoConvertLeadSettings() {
			return [SELECT Id, DeveloperName, Convert_On_Status__c, Convert_To_Account_Name__c, Converted_Opportunity_Name__c FROM Lead_Conversion_Settings__mdt WHERE DeveloperName = 'AutoConvertLeadOnQualified'];
		}

		public List<Configuration_Label__mdt> getConfigurationLabelsByType(String configurationType) {
			return [SELECT Id, DeveloperName, Label_Value__c FROM Configuration_Label__mdt WHERE Label_Type__c = :configurationType];
		}

		public List<Partner_User_Configuration__mdt> getPartnerUserConfigurations() {
			return [SELECT Id, DeveloperName, Contact_Title__c, Related_Segment__c, Partner_Profile__c, Partner_Role__c FROM Partner_User_Configuration__mdt];
		}
	}
}