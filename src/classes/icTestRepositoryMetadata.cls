@isTest
public with sharing class icTestRepositoryMetadata {

	static testMethod void test_getAutoConvertLeadSettings() {
		icRepositoryMetadata.IClass repository = (icRepositoryMetadata.IClass) icObjectFactory.GetSingletonInstance('icRepositoryMetadata');

		List<Lead_Conversion_Settings__mdt> testResult = repository.getAutoConvertLeadSettings();
	}

	static testMethod void test_getConfigurationLabelsByType() {
		icRepositoryMetadata.IClass repository = (icRepositoryMetadata.IClass) icObjectFactory.GetSingletonInstance('icRepositoryMetadata');

		List<Configuration_Label__mdt> testResult = repository.getConfigurationLabelsByType('configType');
	}

	static testMethod void test_getPartnerUserConfigurations() {
		icRepositoryMetadata.IClass repository = (icRepositoryMetadata.IClass) icObjectFactory.GetSingletonInstance('icRepositoryMetadata');

		List<Partner_User_Configuration__mdt> testResult = repository.getPartnerUserConfigurations();
	}
}