@isTest
public class icRepositoryAccountMock implements icRepositoryAccount.IClass{

	public List<Account> getAccountByName(String accountName) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAccountByName');
		params.put('accountName', accountName);
		Return (List<Account>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAccountByName');
	}

	public List<Account> getPartnerAccountsFromList(Set<String> accountIds) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerAccountsFromList');
		params.put('accountIds', accountIds);
		Return (List<Account>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerAccountsFromList');
	}
}