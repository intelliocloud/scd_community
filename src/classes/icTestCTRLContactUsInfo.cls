@isTest
public with sharing class icTestCTRLContactUsInfo {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icBusinessLogicUser', new icBusinessLogicUserMock());
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icBusinessLogicContact', new icBusinessLogicContactMock());
	}

	static testMethod void test_getContactUsInfo() {
		initTest();

		User testUser = new User(
			LastName = 'Test Last User',
			FirstName = 'Test First User',
			Title = 'Title',
			Phone = '444-444-4444',
			MobilePhone = '444-444-4444'
		);
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'getUserInfoById', testUser);

		Account newAccount = new Account(Name = 'new account');
		insert newAccount;
		Contact newContact = new Contact(
			FirstName = 'first name',
			LastName = 'last name',
			AccountId = newAccount.Id
		);
		insert newContact;
		Contact testContact = [SELECT Id, FirstName, LastName, AccountId, Account.OwnerId FROM Contact WHERE Id = :newContact.Id];
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicContactMock', 'getContactInfoById', testContact);

		icDTOContactUsInfo testResult = icCTRLContactUsInfo.getContactUsInfo();
	}
}