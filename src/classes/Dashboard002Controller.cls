/********************************************************************************************************************************
@purpose: Dashboard002Controller Controller
@created date: 04-Aug-2016.
*********************************************************************************************************************************/
global class Dashboard002Controller{
    //Consctructor
    global Dashboard002Controller(ApexPages.StandardController controller){
    
    }
    
    /**
     *@purpose : To fetch account data and it'd related opportunity data of select year and previous Year Data.
     *@param   : a) yearNo : selected year  b) monthNo : selected month  c) accountId
     *@return  : DeakerReportWrapper : Dealer report wrapper.
    **/
    @RemoteAction
    global static DeakerReportWrapper generateReport(integer yearNo, integer monthNo, Id accountId ){
        
        DeakerReportWrapper dealerReportData = new DeakerReportWrapper();
        try{ 
            // Populate current year summary information.
            dealerReportData.currentYearSummarizeData = getSummarizeOpportunityData( yearNo, 
                                                                                     monthNo, 
                                                                                     accountId, 
                                                                                     new summarizeOpportunityWrapper() );
            
            // Populate previous year summary information.
            dealerReportData.prevYearSummarizeData = getSummarizeOpportunityData( yearNo-1, 
                                                                                  monthNo, 
                                                                                  accountId, 
                                                                                  new summarizeOpportunityWrapper());
            //Populate cuurent account information                                                                    
            dealerReportData = getDealreReportData(accountId, dealerReportData);
                dealerReportData.isSuccess = true;
        }catch(Exception exp){
            dealerReportData.isSuccess = false;
            dealerReportData.errorMsg = exp.getMessage();
        }                                                                                                                
        return dealerReportData;
    }
    
    /**
     *@purpose: To get dealerReportData (Account Data) 
     *@param  : a) dealerReportData : Wrapper for account b) accountId
     *@return : dealerReportData.
     */
    private static DeakerReportWrapper getDealreReportData(Id accountId, DeakerReportWrapper dealerReportData){
        
        Account account = [SELECT Id, AnnualRevenue, Name, AutoWorx_no__c
                           FROM Account
                           WHERE Id =: accountId];
        
        dealerReportData.objCurrentYear = account.AnnualRevenue;
        dealerReportData.autoWorxNoStr = account.AutoWorx_no__c;
    
        dealerReportData.currentYearTrend = (account.AnnualRevenue == null || account.AnnualRevenue == 0 ? 0
                                                :(dealerReportData.currentYearSummarizeData.totalVolume/account.AnnualRevenue)*100);
        
        dealerReportData.lookChangePer = calculatePercentage(dealerReportData.prevYearSummarizeData.look, 
                                                             dealerReportData.currentYearSummarizeData.look );
        
        
        dealerReportData.approvalChangePer = calculatePercentage(dealerReportData.prevYearSummarizeData.approval, 
                                                                 dealerReportData.currentYearSummarizeData.approval );
        
        
        dealerReportData.bookChangePer = calculatePercentage(dealerReportData.prevYearSummarizeData.book, 
                                                             dealerReportData.currentYearSummarizeData.book );
                                         
        dealerReportData.averageLoanChangePer = calculatePercentage(dealerReportData.prevYearSummarizeData.averageLoan, 
                                                                    dealerReportData.currentYearSummarizeData.averageLoan );
                                                    

        dealerReportData.totalVolumeChangePer = calculatePercentage(dealerReportData.prevYearSummarizeData.totalVolume, 
                                                                   dealerReportData.currentYearSummarizeData.totalVolume );
        
                                                    
        return dealerReportData;
    }
    
    /**
     *@purpose : To calculatePercentage
     *@param   : a)prevYearData b)currentYearData
     *@return  : Percentage
     **/
    private static Decimal calculatePercentage(Decimal prevYearData, Decimal currentYearData){
        Decimal percentage = 0;
        
        if(prevYearData != null && prevYearData == 0){
            percentage = 0;
        }else if(currentYearData != null){
            percentage =  ((currentYearData - prevYearData)/prevYearData)*100;
        }
        
        return percentage;
    }
   
    /**
     *@purpose : To get summarizeData data of opportunity related to account
     *@param   : a) yearNo : selected year  b) monthNo : selected month  c) accountId d)summarizeOpportunityWrapper
     *@return  : summarizeOpportunityWrapper.
     */
    private static summarizeOpportunityWrapper getSummarizeOpportunityData( integer yearNo, 
                                                                            integer monthNo, 
                                                                            Id accountId, 
                                                                            summarizeOpportunityWrapper summarizeData ) {
        
        
        Date toDate = Date.newInstance(yearNo, monthNo, Date.daysInMonth(yearNo, monthNo));
        Date fromDate = Date.newInstance(yearNo, 1, 1);
        System.debug('toDate=='+toDate);
        System.debug('fromDate=='+fromDate);
                        
        for(Opportunity oppRec: [SELECT Id, DESJ_Look_all__c, DESJ_approved_all__c, 
                                        DESJ_book_all__c, Avg_Total_Amount_Financed__c,
                                        Amount
                                 FROM Opportunity
                                 WHERE AccountId =: accountId AND
                                       RecordType.DeveloperName  = 'Monthly_AutoWorx_DT_Route_One' AND
                                       CloseDate >=: fromDate AND
                                       CloseDate <=: toDate]) {
                                 
            summarizeData.look += oppRec.DESJ_Look_all__c == null? 0 : oppRec.DESJ_Look_all__c;
            summarizeData.approval += oppRec.DESJ_approved_all__c == null? 0 : oppRec.DESJ_approved_all__c;
            summarizeData.book += oppRec.DESJ_book_all__c == null? 0 : oppRec.DESJ_book_all__c;
            summarizeData.totalVolume += oppRec.Amount == null? 0 : oppRec.Amount;
            
        }
        
        summarizeData.averageLoan = summarizeData.book !=0 ? summarizeData.totalVolume/summarizeData.book:0;
        
        return summarizeData;
    }
    
    /**
     *@purpose : Dealer wrapper class
    **/
    global class DeakerReportWrapper extends resultWrapper {
        
        public String selectedDateStr;
        public summarizeOpportunityWrapper prevYearSummarizeData;
        public summarizeOpportunityWrapper currentYearSummarizeData;
        public String autoWorxNoStr;
        public Decimal objCurrentYear;
        public Decimal lookChangePer;
        public Decimal approvalChangePer;
        public Decimal bookChangePer;
        public Decimal averageLoanChangePer;
        public Decimal totalVolumeChangePer;
        public Decimal currentYearTrend;
    }
    
    public virtual Class resultWrapper{
        public String errorMsg;
        public Boolean isSuccess;
    }
    
    /**
     *@purpose : Opportunity wrapper class
    **/
    public class summarizeOpportunityWrapper {
        public Decimal look;
        public Decimal approval;
        public Decimal book;
        public Decimal averageLoan;
        public Decimal totalVolume;
        
        public summarizeOpportunityWrapper(){
        
            look = 0;
            approval = 0;
            book = 0;
            averageLoan = 0;
            totalVolume = 0;
        }
    }
}