/**
    @ Purpose        :- Test class for DealerLoansController.   
*/

@isTest 
Private class DealerLoansControllerTest{

    /*
        @ Purpose     : To setup test data. 
    */
    @testSetup
    static void setupTestData(){
    
        // create account and Opportunity 
        createOpportunityRecord(createAccountRecord());
    }
    
    /*
        @ Purpose     : Validate DealerLoansController. 
    */
    static testmethod void validateDealerLoansController() {
    
        Account recordAccount = [SELECT Id FROM Account WHERE Name = 'xyz'];
        
        // set page add account id
        PageReference dealerTrackMonthlyLoansPage = Page.DealerTrackMonthlyLoans;
        dealerTrackMonthlyLoansPage.getParameters().put('id',recordAccount.Id);
        Test.setCurrentPage(dealerTrackMonthlyLoansPage);

        // call controller methods
        ApexPages.StandardController sController = new ApexPages.StandardController(recordAccount );
        DealerLoansController objDealerLoansController = new DealerLoansController(sController);
        
        objDealerLoansController.getDealerTrackMonthlyLoans();
        System.assertEquals(true,objDealerLoansController.dealerLoansJSON != null);
        
        objDealerLoansController.dealerLoansJSON = null;
        objDealerLoansController.getAWXMonthlyPerformanceOne();
        System.assertEquals(true,objDealerLoansController.dealerLoansJSON != null);
       
    
    }
    
    /*
        @ Purpose     : Creat Account record. 
        @ Return Type   : String [ Account id ].
    */     
    static String createAccountRecord(){
        Account recordAccount = new Account( Name = 'xyz' );
        insert recordAccount;
        return recordAccount.Id;
    }
   
    /*
         @ Purpose     : Creat Opportunity record. 
         @ Parameter   : String [ Account id ].
    */
    static void createOpportunityRecord(String accountId){
   
       // Insert Opportunity with record type 'DealerTrackMonthlyLoans'
       RecordType opportunityRecordType = [SELECT Id 
                                           FROM RecordType 
                                           WHERE 
                                               SobjectType = 'Opportunity' AND 
                                               DeveloperName = 'DealerTrackMonthlyOpp' LIMIT 1];
       Date dateValue = system.today();                                       
       Opportunity recordOpportunity = new Opportunity( RecordType = opportunityRecordType,
                                     Name = 'test',   
                                     Amount = 100.00, 
                                     CloseDate = dateValue.addDays(20),
                                     DESJ_Look_all__c = 100.00, 
                                     DESJ_approved_all__c = 100.00, 
                                     DESJ_book_all__c =  100.00,
                                     StageName ='Look', 
                                     AccountId = accountId
                                     );
       insert recordOpportunity ;
       
       // Insert Opportunity with record type 'SAS-AWXMonthlyPerformance01'                            
       opportunityRecordType = [SELECT Id 
                                FROM RecordType 
                                WHERE 
                                    SobjectType = 'Opportunity' AND 
                                    DeveloperName = 'Monthly_AutoWorx_DT_Route_One' LIMIT 1];
       recordOpportunity.id = null;
       recordOpportunity.RecordType = opportunityRecordType;    
       insert recordOpportunity ;   
   }
}