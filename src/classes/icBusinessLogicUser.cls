public class icBusinessLogicUser implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		User getUserInfoById(String userId);
		List<User> getPartnerUsersByContactIds(Set<String> contactIds);
		Map<String, User> getMapPartnerUserByContactId(List<User> users);
		List<Profile> getPartnerProfile();
		Map<String, Profile> getMapProfileByName(List<Profile> profiles);
		List<UserRole> getPartnerUserRolesByAccountIds(Set<String> accountIds);
		Map<String, List<UserRole>> getMapPartnerUserRolesByAccountId(List<UserRole> roles);
		String generateUserAliasFromName(String firstName, String lastName);
		void createUsers(List<User> newUsers);
		void editUsers(List<User> users);
	}

	public class Impl implements IClass {

		icRepositoryUser.IClass repository = (icRepositoryUser.IClass) icObjectFactory.GetSingletonInstance('icRepositoryUser');

		public User getUserInfoById(String userId) {
			return repository.getUserInfoById(userId);
		}

		public List<User> getPartnerUsersByContactIds(Set<String> contactIds) {
			return repository.getPartnerUsersByContactIds(contactIds);
		}

		public Map<String, User> getMapPartnerUserByContactId(List<User> users) {
			Map<String, User> returnMap = new Map<String, User>();
			for(User thisUser : users) {
				returnMap.put(thisUser.ContactId, thisUSer);
			}
			return returnMap;
		}

		public List<Profile> getPartnerProfile() {
			return repository.getPartnerProfile();
		}

		public Map<String, Profile> getMapProfileByName(List<Profile> profiles) {
			Map<String, Profile> mapProfilesByName = new Map<String, Profile>();
			for(Profile thisProfile : profiles) {
				mapProfilesByName.put(thisProfile.Name, thisProfile);
			}
			return mapProfilesByName;
		}

		public List<UserRole> getPartnerUserRolesByAccountIds(Set<String> accountIds) {
			return repository.getPartnerUserRolesByAccountIds(accountIds);
		}

		public Map<String, List<UserRole>> getMapPartnerUserRolesByAccountId(List<UserRole> roles) {
			Map<String, List<UserRole>> mapPartnerUserRolesByAccountId = new Map<String, List<UserRole>>();
			for(UserRole thisRole : roles) {
				List<UserRole> thisAccountUserRole = mapPartnerUserRolesByAccountId.get(thisRole.PortalAccountId);
				if(thisAccountUserRole == null) {
					thisAccountUserRole = new List<UserRole>();
				}
				thisAccountUserRole.add(thisRole);
				mapPartnerUserRolesByAccountId.put(thisRole.PortalAccountId, thisAccountUserRole);
			}
			return mapPartnerUserRolesByAccountId;
		}

		public String generateUserAliasFromName(String firstName, String lastName) {
			String returnAlias = lastName.left(4);
			if(String.isNotBlank(firstName)) {
				returnAlias += firstName.left(4);
			}
			return returnAlias;
		}

		public void createUsers(List<User> newUsers) {
			repository.createUsers(newUsers);
		}

		public void editUsers(List<User> users) {
			repository.editUsers(users);
		}
	}
}