@isTest
public with sharing class icTestBusinessLogicAccount {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepositoryAccount', new icRepositoryAccountMock());
	}

	static testMethod void test_getAccountByName() {
		initTest();

		Account testAccount = new Account(Name = 'Test Account');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryAccountMock', 'getAccountByName', new List<Account> {testAccount});

		icBusinessLogicAccount.IClass businessLogic = (icBusinessLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAccount');
		Account testResult = businessLogic.getAccountByName('accountName');
	}

	static testMethod void test_getPartnerAccountsFromList() {
		initTest();

		Account testAccount = new Account(Name = 'Test Account');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryAccountMock', 'getPartnerAccountsFromList', new List<Account> {testAccount});

		icBusinessLogicAccount.IClass businessLogic = (icBusinessLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAccount');
		List<Account> testResult = businessLogic.getPartnerAccountsFromList(new Set<String>());
	}

	static testMethod void test_getMapAccountById() {
		Account testAccount = new Account(Name = 'Test Account');

		icBusinessLogicAccount.IClass businessLogic = (icBusinessLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAccount');

		Map<String, Account> testResult = businessLogic.getMapAccountById(new List<Account> {testAccount});
	}
}