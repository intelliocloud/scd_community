@isTest
public with sharing class icTestTriggerUser {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icTriggerHandlerUser', new icTriggerHandlerUserMock());
	}

	static testMethod void test_getProjectInfoForProductSelection() {
		initTest();

		Profile thisProfile = [SELECT Id, Name FROM Profile WHERE UserType = 'Standard' LIMIT 1];

		User newUser = new User(
			Username = 'new.user@username.com',
			ProfileId = thisProfile.Id,
			Alias = 'alias',
			Email = 'new.user@username.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'FirstName',
			LastName = 'LastName',
			CommunityNickname = 'alias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		insert newUser;

		update newUser;
	}
}