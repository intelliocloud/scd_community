/****************************************************************************
 * @purpose: TaskTake for auto populating related to field when clicking Task button from any object
 * @Created Date: 09/08/16
 ***************************************************************************/

public class TaskTake{

public string whoidd;
public string whatId; 
public string retId; 
public string original;
public string whoiddstr;
public string Whatidstr; 
public string lookupname='1';
public string lookupid;
public string recordtypeidd;
public String remainder;
public TaskTake(apexpages.standardcontroller controller)
{
/****************************************************************************
 * @purpose: Try block to handle null pointer exception

 ***************************************************************************/
try
{
original=ApexPages.currentPage().getUrl();
remainder = original.substring(13);
remainder=remainder.replace('save_new=1&','');
whoidd=Apexpages.currentpage().getparameters().get('who_id'); 
whatId=Apexpages.currentpage().getparameters().get('what_id'); 
retId=Apexpages.currentpage().getparameters().get('retURL'); 
recordtypeidd =Apexpages.currentpage().getparameters().get('RecordType');
if(whoidd!=null){ 
WhoiddStr=whoidd.substring(0,3); 
} 
else if(whatId!=null){ 
Whatidstr=whatId.substring(0,3); 
} 
/************************
Condition if Task is calling from contact
*************************/
if(WhoiddStr=='003'){
list<contact> aa=[select Accountid from contact where id=:whoidd];
list<Account> c=[select Name,id from Account  where id=:aa[0].Accountid];

system.debug('==========='+c);
lookupname=c[0].name;
lookupid=c[0].id;

}
/************************
Condition if Task is calling from Account
*************************/
if(Whatidstr=='001'){ 

list<Account> cc=[select Name,id from Account where id=:whatId]; 
lookupname=cc[0].name; 
lookupid=cc[0].id; 
} 

}
catch(Exception e){
system.debug('==============exception======='+e);
}


}

public pagereference pf()
{
Pagereference newpage;
/************************
Condition if Task is calling from contact
*************************/
if(whoiddstr=='003'){
newpage=new pagereference('/00T/e?nooverride=1&retURL='+retId+'&who_id='+whoidd+'&RecordType='+recordtypeidd+'&tsk3_lkid='+lookupid+'&tsk3_mlktp=001&ent=Task&tsk3='+lookupname+'&IsReminderSet='+false);
}
/************************
Condition if Task is calling from account
*************************/

else if(Whatidstr=='001'){ 
newpage = new pagereference('/00T/e?nooverride=1&retURL='+retId+'&what_id='+whatId+'&RecordType='+recordtypeidd+'&tsk3_lkid='+lookupid+'&tsk3_mlktp=001&ent=Task&tsk3='+lookupname+'&IsReminderSet='+false); 
} 

/************************
Condition if event is calling from any other object
*************************/
else{
newpage = new pagereference('/00T/e?nooverride=1'+remainder); 
}
      newpage.setredirect(true);
       return newpage;

}}