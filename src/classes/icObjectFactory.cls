public class icObjectFactory {
	
	public Interface IMockCreator{ 
		object CreateMockInstance(String ClassName);
	}

	@testVisible
	private static Map<string, object> globalObjects = new Map<string, object>();
	private static IMockCreator mockInstantiator = null;

	public static object GetSingletonInstance(String className){
		if(!globalObjects.containsKey(className)){
			Type t = Type.forName(className);
			Object newObj = ((icIClass) t.newInstance()).GetInstance();
			System.Debug('Type Found : ' + t + ' for class : ' + className);
			globalObjects.put(className, newObj);
		}

		return globalObjects.get(className);
	}

	public static void MockSingletonInstance(String className, Object mock){
		globalObjects.put(className, mock);
	}

	public static void SetMockInstantiator(IMockCreator creator){
		mockInstantiator = creator;
	}

	public static void ResetFactory(){
		globalObjects.clear();
	}

	public static object GetInstance(String className){
		if(mockInstantiator == null){
			Type t = Type.forName(className);
			return ((icIClass) t.newInstance()).GetInstance();
		}

		return mockInstantiator.CreateMockInstance(ClassName);
	}

	public static String convertIdToCaseInsensitive(String inputId) {
		string suffix = '';
		integer flags;
		try{
			for (integer i = 0; i < 3; i++) {
				flags = 0;
				for (integer j = 0; j < 5; j++) {
					string c = inputId.substring(i * 5 + j,i * 5 + j + 1);
					if (c.toUpperCase().equals(c) && c >= 'A' && c <= 'Z') {
						flags = flags + (1 << j);
					}
				}
				
				if (flags <= 25) {
					suffix += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.substring(flags,flags+1);
				} else {
					suffix += '012345'.substring(flags - 26, flags-25);
				}
			}
		} catch(Exception exc) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Valid 15 digit Id'));
		}
		String outputId = inputId+suffix;
		return outputId;
	}
}