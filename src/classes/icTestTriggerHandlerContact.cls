@isTest
public with sharing class icTestTriggerHandlerContact {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icBusinessLogicAccount', new icBusinessLogicAccountMock());
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icBusinessLogicUser', new icBusinessLogicUserMock());
	}

	static testMethod void test_onBeforeInsert() {
		List<Contact> oldContacts = new List<Contact>();
		List<Contact> newContacts = new List<Contact>();
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		
		icTriggerHandlerContact.IClass handler = (icTriggerHandlerContact.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerContact');
		handler.onBeforeInsert(newContacts, contactMap);
	}

	static testMethod void test_onAfterInsert() {
		initTest();

		String thisContactId = icTestHelperUtility.getFakeId(Contact.SObjectType);
		String thisAccountId = icTestHelperUtility.getFakeId(Account.SObjectType);

		Account testAccount = new Account(Id = thisAccountId, Name = 'Test Account');
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicAccountMock', 'getPartnerAccountsFromList', new List<Account> {testAccount});

		Map<String, Account> mockMapAccountById = new Map<String, Account>();
		mockMapAccountById.put(thisAccountId, testAccount);
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicAccountMock', 'getMapAccountById', mockMapAccountById);

		List<Profile> allPartnerProfiles = [SELECT Id, Name FROM Profile WHERE UserType = 'PowerPartner'];
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'getPartnerProfile', allPartnerProfiles);

		Map<String, Profile> mapProfilesByName = new Map<String, Profile>();
		for(Profile thisProfile : allPartnerProfiles) {
			mapProfilesByName.put(thisProfile.Name, thisProfile);
		}
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'getMapProfileByName', mapProfilesByName);

		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'generateUserAliasFromName', 'uidAlias');

		List<UserRole> allPartnerRoles = [SELECT Id, Name, PortalAccountId FROM UserRole WHERE PortalAccountId != NULL];
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'getPartnerUserRolesByAccountIds', allPartnerRoles);

		Map<String, List<UserRole>> mapPartnerUserRolesByAccountId = new Map<String, List<UserRole>>();
		for(UserRole thisRole : allPartnerRoles) {
			List<UserRole> thisAccountUserRole = mapPartnerUserRolesByAccountId.get(thisAccountId);
			if(thisAccountUserRole == null) {
				thisAccountUserRole = new List<UserRole>();
			}
			thisAccountUserRole.add(thisRole);
			mapPartnerUserRolesByAccountId.put(thisAccountId, thisAccountUserRole);
		}
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'getMapPartnerUserRolesByAccountId', mapPartnerUserRolesByAccountId);

		Contact thisContact = new Contact(Id = thisContactId
			,FirstName = 'First'
			,LastNAme = 'Last'
			,Contact_title__c = 'Title'			
			,AccountId = thisAccountId);

		List<Contact> oldContacts = new List<Contact>();
		oldContacts.add(thisContact);
		List<Contact> newContacts = new List<Contact>();
		newContacts.add(thisContact);
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		contactMap.put(thisContactId, thisContact);

		icTriggerHandlerContact.IClass handler = (icTriggerHandlerContact.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerContact');
		handler.onAfterInsert(oldContacts, newContacts, contactMap);
	}

	static testMethod void test_onBeforeUpdate() {
		List<Contact> oldContacts = new List<Contact>();
		List<Contact> newContacts = new List<Contact>();
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		
		icTriggerHandlerContact.IClass handler = (icTriggerHandlerContact.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerContact');
		handler.onBeforeUpdate(oldContacts, newContacts, contactMap);
	}

	static testMethod void test_onAfterUpdate() {
		List<Contact> oldContacts = new List<Contact>();
		List<Contact> newContacts = new List<Contact>();
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		
		icTriggerHandlerContact.IClass handler = (icTriggerHandlerContact.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerContact');
		handler.onAfterUpdate(oldContacts, newContacts, contactMap);
	}

	static testMethod void test_onBeforeDelete() {
		List<Contact> oldContacts = new List<Contact>();
		List<Contact> newContacts = new List<Contact>();
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		
		icTriggerHandlerContact.IClass handler = (icTriggerHandlerContact.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerContact');
		handler.onBeforeDelete(oldContacts, contactMap);
	}

	static testMethod void test_onAfterDelete() {
		List<Contact> oldContacts = new List<Contact>();
		List<Contact> newContacts = new List<Contact>();
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		
		icTriggerHandlerContact.IClass handler = (icTriggerHandlerContact.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerContact');
		handler.onAfterDelete(oldContacts, contactMap);
	}

	static testMethod void test_onAfterUndelete() {
		List<Contact> oldContacts = new List<Contact>();
		List<Contact> newContacts = new List<Contact>();
		Map<Id, Contact> contactMap = new Map<Id, Contact>();
		
		icTriggerHandlerContact.IClass handler = (icTriggerHandlerContact.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerContact');
		handler.onAfterUndelete(oldContacts, newContacts, contactMap);
	}
}