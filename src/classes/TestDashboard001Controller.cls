/*******************************************************************************************************************************
 * @purpose: Test Class for TradingChartController
 * @Created Date: 24/3/16
 *******************************************************************************************************************************/
@isTest
private class TestDashboard001Controller {
    
    @testSetup
    private static void createTestDataForDealerReport(){
        Account account = new Account(Name = 'Test Account', AutoWorx_no__c = '001', AnnualRevenue = 100000);
        Insert account;
        List<Opportunity> OpptyList = createOpportunities(50, 'Monthly AutoWorx DT-Route One', account);
        INSERT OpptyList;
    }
    
    private static List<Opportunity> createOpportunities (Integer totalopptyTocreate, String recordTypeName, Account account){
        
        List<Opportunity> OpptyList = new List<Opportunity>();
        Map<String, Id> recordtypeNameToIdMap = buildRecordtypeNameToIdMap('Opportunity');
        Date todaysDate = System.today();
        
        for(Integer i=0; i<totalopptyTocreate; i++){
            
            OpptyList.add(new Opportunity( Name = 'TestOpportunity'+i, DESJ_Look_all__c = i, DESJ_approved_all__c = i,
                                           DESJ_book_all__c = i, Avg_Total_Amount_Financed__c = i, Amount = i,
                                           RecordTypeId = recordtypeNameToIdMap.get(recordTypeName),
                                           AccountId = account.Id, StageName = 'Book', 
                                           CloseDate = Date.newInstance(todaysDate.year(), todaysDate.month(), 1),
                                           Dealer_Approved_Prime_Non_subvented__c = i,
                                           Dealer_Book_Prime_Non_subvented__c = i,
                                           Dealer_Look_Prime_Non_subvented__c = i
                                          ));
                                          
            OpptyList.add(new Opportunity( Name = 'TestOpportunity'+i, DESJ_Look_all__c = i+1, DESJ_approved_all__c = i+1,
                                           DESJ_book_all__c = i+1, Avg_Total_Amount_Financed__c = i+1, Amount = i+1,
                                           RecordTypeId = recordtypeNameToIdMap.get(recordTypeName),
                                           AccountId = account.Id, StageName = 'Book', 
                                           CloseDate = Date.newInstance(todaysDate.year(), todaysDate.month(), 15),
                                           Dealer_Approved_Prime_Non_subvented__c = i+1,
                                           Dealer_Book_Prime_Non_subvented__c = i+1,
                                           Dealer_Look_Prime_Non_subvented__c = i+1
                                          ));
        }
        
        return OpptyList;
    }
    
    private static Map<String, Id> buildRecordtypeNameToIdMap(String SobjectName){
        
        Map<String, Id> recordtypeNameToIdMap = new Map<String, Id>();
        
        for(RecordType recType :[SELECT Id, Name FROM RecordType WHERE SobjectType =: SobjectName]){
            recordtypeNameToIdMap.put(recType.Name, recType.Id);
        }
        
        return recordtypeNameToIdMap;
    }
    
    public static testMethod void testTradingChartController(){
        
        List<Opportunity> opportunityList = [SELECT Name, CloseDate FROM Opportunity];
        Account currentAccount = [SELECT id, Name FROM Account WHERE Name = 'Test Account' LIMIT 1];        
        
        Test.startTest();
                
        ApexPages.StandardController sc = new ApexPages.StandardController(currentAccount);
        System.currentPageReference().getParameters().put('id', currentAccount.id);        
        
        Dashboard001Controller tradingChartInstance = new Dashboard001Controller(sc);  
        tradingChartInstance.getRecordTypes();
        tradingChartInstance.selectedRecordType = 'Monthly_AutoWorx_DT_Route_One';
        tradingChartInstance.selectedRecordTypeLabel = 'Monthly AutoWorx DT-Route One';
        Dashboard001Controller.OpportunityWrapper opptyWrapper = new Dashboard001Controller.OpportunityWrapper();
 
        opptyWrapper = tradingChartInstance.queryOnOpportunity(Date.newInstance(Date.today().year(), Date.today().month(), 1));
        
        Test.stopTest();
        
        System.assertEquals(99.0, opptyWrapper.booktoLook);
        System.assertEquals(99.00, opptyWrapper.approvalToLook);
        System.assertEquals(99.00, opptyWrapper.bookToApproval);
        System.assertEquals(99.00, opptyWrapper.primeBookToLook);
        System.assertEquals(99.00, opptyWrapper.primeApprovalToLook);
        System.assertEquals(99.00, opptyWrapper.primeBookToApproval);
    }
}