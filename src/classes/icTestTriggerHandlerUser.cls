@isTest
public with sharing class icTestTriggerHandlerUser {

	static testMethod void test_onBeforeInsert_default() {
		Profile thisProfile = [SELECT Id, Name FROM Profile WHERE UserType = 'Standard' LIMIT 1];
		String newUserId = icTestHelperUtility.getFakeId(User.SObjectType);
		User newUser = new User(Id = newUserId,
			Username = 'new.user@username.com',
			ProfileId = thisProfile.Id,
			Alias = 'alias',
			Email = 'new.user@username.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'FirstName',
			LastName = 'LastName',
			CommunityNickname = 'alias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'fr_CA',
			LanguageLocaleKey = 'fr_CA');
		
		List<User> newUsers = new List<User>();
		newUsers.add(newUser);
		Map<Id, User> userMap = new Map<Id, User>();
		userMap.put(newUserId, newUser);
		
		icTriggerHandlerUser.IClass handler = (icTriggerHandlerUser.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerUser');
		handler.onBeforeInsert(newUsers, userMap);
	}

	static testMethod void test_onBeforeInsert_en() {
		Profile thisProfile = [SELECT Id, Name FROM Profile WHERE UserType = 'Standard' LIMIT 1];
		String newUserId = icTestHelperUtility.getFakeId(User.SObjectType);
		User newUser = new User(Id = newUserId,
			Username = 'new.user@username.com',
			ProfileId = thisProfile.Id,
			Alias = 'alias',
			Email = 'new.user@username.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'FirstName',
			LastName = 'LastName',
			CommunityNickname = 'alias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_CA',
			LanguageLocaleKey = 'en_CA');

		List<User> newUsers = new List<User>();
		newUsers.add(newUser);
		Map<Id, User> userMap = new Map<Id, User>();
		userMap.put(newUserId, newUser);
		
		icTriggerHandlerUser.IClass handler = (icTriggerHandlerUser.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerUser');
		handler.onBeforeInsert(newUsers, userMap);
	}

	static testMethod void test_onAfterInsert() {
		List<User> oldUsers = new List<User>();
		List<User> newUsers = new List<User>();
		Map<Id, User> userMap = new Map<Id, User>();
		
		icTriggerHandlerUser.IClass handler = (icTriggerHandlerUser.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerUser');
		handler.onAfterInsert(oldUsers, newUsers, userMap);
	}

	static testMethod void test_onBeforeUpdate_default() {
		Profile thisProfile = [SELECT Id, Name FROM Profile WHERE UserType = 'Standard' LIMIT 1];
		String newUserId = icTestHelperUtility.getFakeId(User.SObjectType);
		User oldUser = new User(Id = newUserId,
			Username = 'new.user@username.com',
			ProfileId = thisProfile.Id,
			Alias = 'alias',
			Email = 'new.user@username.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'FirstName',
			LastName = 'LastName',
			CommunityNickname = 'alias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_CA',
			LanguageLocaleKey = 'en_CA');
		User newUser = new User(Id = newUserId,
			Username = 'new.user@username.com',
			ProfileId = thisProfile.Id,
			Alias = 'alias',
			Email = 'new.user@username.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'FirstName',
			LastName = 'LastName',
			CommunityNickname = 'alias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'fr_CA',
			LanguageLocaleKey = 'fr_CA');

		List<User> oldUsers = new List<User>();
		oldUsers.add(oldUser);
		List<User> newUsers = new List<User>();
		newUsers.add(newUser);
		Map<Id, User> userMap = new Map<Id, User>();
		userMap.put(newUserId, newUser);
		
		icTriggerHandlerUser.IClass handler = (icTriggerHandlerUser.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerUser');
		handler.onBeforeUpdate(oldUsers, newUsers, userMap);
	}

	static testMethod void test_onBeforeUpdate_en() {
		Profile thisProfile = [SELECT Id, Name FROM Profile WHERE UserType = 'Standard' LIMIT 1];
		String newUserId = icTestHelperUtility.getFakeId(User.SObjectType);
		User oldUser = new User(Id = newUserId,
			Username = 'new.user@username.com',
			ProfileId = thisProfile.Id,
			Alias = 'alias',
			Email = 'new.user@username.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'FirstName',
			LastName = 'LastName',
			CommunityNickname = 'alias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'fr_CA',
			LanguageLocaleKey = 'fr_CA');
		User newUser = new User(Id = newUserId,
			Username = 'new.user@username.com',
			ProfileId = thisProfile.Id,
			Alias = 'alias',
			Email = 'new.user@username.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'FirstName',
			LastName = 'LastName',
			CommunityNickname = 'alias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_CA',
			LanguageLocaleKey = 'en_CA');

		List<User> oldUsers = new List<User>();
		oldUsers.add(oldUser);
		List<User> newUsers = new List<User>();
		newUsers.add(newUser);
		Map<Id, User> userMap = new Map<Id, User>();
		userMap.put(newUserId, newUser);
		
		icTriggerHandlerUser.IClass handler = (icTriggerHandlerUser.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerUser');
		handler.onBeforeUpdate(oldUsers, newUsers, userMap);
	}

	static testMethod void test_onAfterUpdate() {
		List<User> oldUsers = new List<User>();
		List<User> newUsers = new List<User>();
		Map<Id, User> userMap = new Map<Id, User>();
		
		icTriggerHandlerUser.IClass handler = (icTriggerHandlerUser.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerUser');
		handler.onAfterUpdate(oldUsers, newUsers, userMap);
	}
}