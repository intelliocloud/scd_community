public class icBusinessLogicMetadata implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		Lead_Conversion_Settings__mdt getAutoConvertLeadSettings();
		List<Configuration_Label__mdt> getConfigurationLabelsByType(String configurationType);
		List<Partner_User_Configuration__mdt> getPartnerUserConfigurations();
		Map<String, Partner_User_Configuration__mdt> getPartnerUserConfigurations(List<Partner_User_Configuration__mdt> partnerUserConfigurations);
	}

	public class Impl implements IClass {

		icRepositoryMetadata.IClass repository = (icRepositoryMetadata.IClass) icObjectFactory.GetSingletonInstance('icRepositoryMetadata');

		public Lead_Conversion_Settings__mdt getAutoConvertLeadSettings() {
			Lead_Conversion_Settings__mdt returnSettings;
			List<Lead_Conversion_Settings__mdt> leadConvertSettings = repository.getAutoConvertLeadSettings();
			if(!leadConvertSettings.isEmpty()) {
				returnSettings = leadConvertSettings[0];
			}
			return returnSettings;
		}

		public List<Configuration_Label__mdt> getConfigurationLabelsByType(String configurationType) {			
			return repository.getConfigurationLabelsByType(configurationType);
		}

		public List<Partner_User_Configuration__mdt> getPartnerUserConfigurations() {
			return repository.getPartnerUserConfigurations();
		}

		public Map<String, Partner_User_Configuration__mdt> getPartnerUserConfigurations(List<Partner_User_Configuration__mdt> partnerUserConfigurations) {
			Map<String, Partner_User_Configuration__mdt> mapPartnerConfigByTitleSegmentation = new Map<String, Partner_User_Configuration__mdt>();
			for(Partner_User_Configuration__mdt partnerUserConfiguration : partnerUserConfigurations) {
				mapPartnerConfigByTitleSegmentation.put(partnerUserConfiguration.Contact_Title__c + '_' + partnerUserConfiguration.Related_Segment__c, partnerUserConfiguration);
			}
			return mapPartnerConfigByTitleSegmentation;
		}
	}
}