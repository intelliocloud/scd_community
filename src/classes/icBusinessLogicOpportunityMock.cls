@isTest
public class icBusinessLogicOpportunityMock implements icBusinessLogicOpportunity.IClass{

	public Opportunity generateOpportunityForLead(Lead thisLead, String opportunityName) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'generateOpportunityForLead');
		params.put('thisLead', thisLead);
		params.put('opportunityName', opportunityName);
		Return (Opportunity) icTestMockUtilities.Tracer.GetReturnValue(this, 'generateOpportunityForLead');
	}

	public void createOpportunities(List<Opportunity> newOpportunities) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'createOpportunities');
		params.put('newOpportunities', newOpportunities);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'createOpportunities');
	}
}