/****************************************************************************
 * @purpose: Trigger Handler for Account Object
 * @Created Date: 17/3/16
 ***************************************************************************/
public class AccountTriggerHandler {
    
  /*******************************************************************
   * @purpose: Function Called Before Insert of Account Record
   * @param: List of Account Records
   *******************************************************************/
    public static void isBeforeInsert(List<Account> accountList){
        
        //Function to validate if city is in list of given Cities
        populateStandardCityAndProvinceCode(accountList);
    }

    
  /*******************************************************************
   * @purpose: Function Called Before Update of Account Record
   * @param: List of Account Records
   *******************************************************************/    
    public static void isBeforeUpdate(List<Account> accountList, Map<Id, Account> oldAccount){
        
        //Function to validate if city is in list of given Cities
        onUpdatePopulateCityAndProvinceCode(accountList, oldAccount);
    }
    
    
    /***************************************************************************************************************
    * @purpose: Check whether Accout.BillingCity and Accout.ShippingCity value changed, if yes repopulate value
    * @param  : a) accounts
    *           b) oldAccountMap
    **************************************************************************************************************/
    private static void onUpdatePopulateCityAndProvinceCode(List<Account> accounts, Map<Id, Account> oldAccountMap) {
        
        List<Account> accountsToUpdate = new List<Account>();
        Account oldAccount;
        Boolean isAccountCityChanged;
        for(Account accountRec: accounts) {
            
            if(!accountRec.isUpload__c){
                oldAccount = oldAccountMap.get(accountRec.id);
                isAccountCityChanged = false;
                
                if( accountRec.shippingCity != oldAccount.shippingCity || 
                   accountRec.BillingCity != oldAccount.BillingCity ) {
                       
                       isAccountCityChanged = true;
                   }
                
                if(accountRec.city1__c != oldAccount.city1__c) {
                    
                    isAccountCityChanged = true;
                    if(String.isBlank(accountRec.city1__c))
                        accountRec.shippingCity = null;
                }
                
                if(accountRec.city2__c != oldAccount.city2__c) {
                    
                    isAccountCityChanged = true;
                    if(String.isBlank(accountRec.city2__c)) 
                        accountRec.BillingCity = null;
                }
                
                if(isAccountCityChanged)
                    accountsToUpdate.add(accountRec);                
            }            
        }
        
        if(accountsToUpdate.size() > 0)
            populateStandardCityAndProvinceCode(accountsToUpdate);
        
    }
    
   /***************************************************************************************************************
    * @purpose: Populate from 
    * @param  : List of Account Records
    **************************************************************************************************************/     
    private static void populateStandardCityAndProvinceCode(List<Account> accountList) {        
        
        Map<Id, City__c> cityMap = getCityMap(accountList);
        City__c cityRec;
        for(Account accountRec: accountList){
            if(!accountRec.isUpload__c){
                if(String.isNotBlank(accountRec.city1__c)){
                    
                    cityRec = cityMap.get(accountRec.city1__c);
                    accountRec.shippingCity = cityRec.Name;
                    accountRec.Province_Code__c = cityRec.Province_Code__c;
                }
                else if(String.isNotBlank(accountRec.shippingCity))
                    accountRec.shippingCity.addError('User should add Shipping city value to City1 field');
                
                if(String.isNotBlank(accountRec.city2__c)){
                    
                    cityRec = cityMap.get(accountRec.city2__c);
                    accountRec.BillingCity = cityRec.Name;
                    accountRec.Billing_Province_Code__c = cityRec.Province_Code__c;
                    
                }
                else if(String.isNotBlank(accountRec.BillingCity))
                    accountRec.BillingCity.addError('User should add Billing city value to City2 field');   
            }
        }
        
    }
    
    
    /***************************************************************************************************************
    * @purpose: Fetch City name and province code from City Object.
    * @param  : a) accountList
    * @return : Return map of city object.
    **************************************************************************************************************/ 
    private static Map<Id, City__c> getCityMap(List<Account> accountList){
        
        Set<Id> cityIdSet = new Set<Id>();   
        for(Account accountRec: accountList){
            
            if(String.isNotBlank(accountRec.city1__C))
                cityIdSet.add(accountRec.city1__C);
            
            if(String.isNotBlank(accountRec.city2__C))
                cityIdSet.add(accountRec.city2__C);
        }
        
        if(cityIdSet.size() > 0)
            return new Map<Id, City__c>([SELECT Id, Name, Province_Code__c FROM City__c WHERE ID IN: cityIdSet]);
        
        return new Map<Id, City__c>();
    }
}