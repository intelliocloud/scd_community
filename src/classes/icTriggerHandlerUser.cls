public class icTriggerHandlerUser implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		void onBeforeInsert(List<User> newUsers, Map<Id, User> userMap);
		void onAfterInsert(List<User> oldUsers, List<User> newUsers, Map<Id, User> userMap);
		void onBeforeUpdate(List<User> oldUsers, List<User> newUsers, Map<Id, User> userMap);
		void onAfterUpdate(List<User> oldUsers, List<User> newUsers, Map<Id, User> userMap);		
	}

	public class Impl implements IClass {

		icBusinessLogicMetadata.IClass blMetadata = (icBusinessLogicMetadata.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicMetadata');
		icBusinessLogicAccount.IClass blAccount = (icBusinessLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAccount');
		icBusinessLogicContact.IClass blContact = (icBusinessLogicContact.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicContact');
		icBusinessLogicOpportunity.IClass blOpportunity = (icBusinessLogicOpportunity.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicOpportunity');
		icBusinessLogicUser.IClass blUser = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');

		public void onBeforeInsert(List<User> newUsers, Map<Id, User> userMap) {
			for(User thisUser : newUsers) {
				thisUser.Prefered_Language__c = 'French';
				String languageLocalKey = thisUser.LanguageLocaleKey.toLowerCase();
				if(languageLocalKey.contains('en')) {
					thisUser.Prefered_Language__c = 'English';
				}
			}
		}

		public void onAfterInsert(List<User> oldUsers, List<User> newUsers, Map<Id, User> userMap) {
		}

		public void onBeforeUpdate(List<User> oldUsers, List<User> newUsers, Map<Id, User> userMap) {
			for(User oldUser : oldUsers) {
				User newUser = userMap.get(oldUser.Id);
				if(oldUser.LanguageLocaleKey != newUser.LanguageLocaleKey) {
					newUser.Prefered_Language__c = 'French';
					String languageLocalKey = newUser.LanguageLocaleKey.toLowerCase();
					if(languageLocalKey.contains('en')) {
						newUser.Prefered_Language__c = 'English';
					}
				}
			}
		}

		public void onAfterUpdate(List<User> oldUsers, List<User> newUsers, Map<Id, User> userMap) {			
		}
	}
}