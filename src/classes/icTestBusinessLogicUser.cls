@isTest
public with sharing class icTestBusinessLogicUser {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepositoryUser', new icRepositoryUserMock());
	}

	static testMethod void test_getUserInfoById() {
		initTest();

		User testUser = new User(LastName = 'Test User');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryUserMock', 'getUserInfoById', testUser);

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		User testResult = businessLogic.getUserInfoById('userId');
	}

	static testMethod void test_getPartnerUsersByContactIds() {
		initTest();

		User testUser = new User(LastName = 'Test User');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryUserMock', 'getPartnerUsersByContactIds', new List<User> {testUser});

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		List<User> testResult = businessLogic.getPartnerUsersByContactIds(new Set<String>());
	}

	static testMethod void test_getMapPartnerUserByContactId() {
		User testUser = new User(LastName = 'Test Last User', FirstName = 'Test First User', ContactId = icTestHelperUtility.getFakeId(User.SObjectType));

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		Map<String, User> testResult = businessLogic.getMapPartnerUserByContactId(new List<User> {testUser});
	}

	static testMethod void test_getPartnerProfile() {
		initTest();

		Profile testProfile = new Profile(Name = 'Test Profile');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryUserMock', 'getPartnerProfile', new List<Profile> {testProfile});

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		List<Profile> testResult = businessLogic.getPartnerProfile();
	}

	static testMethod void test_getMapProfileByName() {
		Profile testProfile = new Profile(Name = 'Test Profile');

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		Map<String, Profile> testResult = businessLogic.getMapProfileByName(new List<Profile> {testProfile});
	}

	static testMethod void test_getPartnerUserRolesByAccountIds() {
		initTest();

		UserRole testUserRole = new UserRole(Name = 'Test UserRole');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryUserMock', 'getPartnerUserRolesByAccountIds', new List<UserRole> {testUserRole});

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		List<UserRole> testResult = businessLogic.getPartnerUserRolesByAccountIds(new Set<String>());
	}

	static testMethod void test_getMapPartnerUserRolesByAccountId() {
		UserRole testUserRole = new UserRole(Name = 'Test UserRole', PortalAccountId = icTestHelperUtility.getFakeId(Account.sObjectType));

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		Map<String, List<UserRole>> testResult = businessLogic.getMapPartnerUserRolesByAccountId(new List<UserRole> {testUserRole});
	}

	static testMethod void test_generateUserAliasFromName() {
		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		String testResult = businessLogic.generateUserAliasFromName('firstName', 'LastName');
	}

	static testMethod void test_createUsers() {
		initTest();

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		businessLogic.createUsers(new List<User>());
	}

	static testMethod void test_editUsers() {
		initTest();

		icBusinessLogicUser.IClass businessLogic = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');
		businessLogic.editUsers(new List<User>());
	}
}