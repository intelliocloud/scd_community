/**
  * @Description : TmpLoadPerformance__c Trigger Handler test class
  * @Created Date: 28 Nov 2016
 */
@isTest
public class TmpLoadPerformanceTriggerHandlerTest {
    
    static testmethod void testOportunityCreation() {
        List<Account> accounts = new List<Account>();
        for( Integer count = 0; count < 10; count++ ) {
            accounts.add(buildAccount('test ' + count, '168' + count, '123' + count));
        }
        
        for( Integer count = 10; count < 20; count++ ) {
            accounts.add(buildAccount('test ' + count, '123' + count, '168' + count));
        }
        
        insert accounts;
        System.debug('accounts : ' + accounts);
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Monthly AutoWorx DT-Route One').getRecordTypeId();
        List<TmpLoadPerformance__c> loadPerformances = new List<TmpLoadPerformance__c>();
        for( Integer count = 0; count < 200; count++ ) {
            loadPerformances.add(buildLoadPerfomance('168' + count,'test',Date.valueOf('2016-10-1'),'book','abc cde','abc cde',recordTypeId, ''));    
        }
        for( Integer count = 0; count < 200; count++ ) {
            loadPerformances.add(buildLoadPerfomance('198' + count,'test',Date.valueOf('2016-10-1'),'book','abc cde','abc cde',recordTypeId, ''));    
        }
        loadPerformances.add(buildLoadPerfomance('lkl','',Date.valueOf('2016-10-1'),'','','',recordTypeId,''));    
        System.debug('loadPerformances in test: ' + loadPerformances);
        Test.startTest();
        //insert loadPerformances;
        Database.insert(loadPerformances, false);
        Test.stopTest();
        
        Integer insertedAccountsCount = [ SELECT COUNT() FROM Account ];
        Integer insertedContactsCount = [ SELECT COUNT() FROM Contact ];
        Integer insertedOpportunitiesCount = [ SELECT COUNT() FROM Opportunity ];
        
        System.assertEquals(insertedAccountsCount, 400);
        System.assertEquals(insertedContactsCount, 380);
        System.assertEquals(insertedOpportunitiesCount, 400);
    }
    
    static testmethod void testInsertionOpportunitiesWithacount() {
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Monthly AutoWorx DT-Route One').getRecordTypeId();
        List<TmpLoadPerformance__c> loadPerformances = new List<TmpLoadPerformance__c>();
        for( Integer count = 0; count < 200; count++ ) {
            loadPerformances.add(buildLoadPerfomance('198' + count,'test',Date.valueOf('2016-10-1'),'book','abc cde','abc cde',recordTypeId,''));    
        }
        loadPerformances.add(buildLoadPerfomance('lkl','test',Date.valueOf('2016-10-1'),'','test con','test con',recordTypeId,''));    
        System.debug('loadPerformances in test: ' + loadPerformances);
        Test.startTest();
        Database.insert(loadPerformances, false);
        Test.stopTest(); 
        
        Integer insertedAccountsCount = [ SELECT COUNT() FROM Account ];
        Integer insertedContactsCount = [ SELECT COUNT() FROM Contact ];
        Integer insertedOpportunitiesCount = [ SELECT COUNT() FROM Opportunity ];
        
        System.assertEquals(insertedAccountsCount, 200);
        System.assertEquals(insertedContactsCount, 200);
        System.assertEquals(insertedOpportunitiesCount, 200);   
    }
    
    //--MDT - Monthly Dealer Track record type
    static testmethod void testMDTOpportunitiesInsertion() {
        List<Account> accounts = new List<Account>();
        for( Integer count = 0; count < 10; count++ ) {
            accounts.add(buildAccount('test ' + count, '168' + count, '123' + count));
        }
        
        for( Integer count = 10; count < 20; count++ ) {
            accounts.add(buildAccount('test ' + count, '123' + count, '168' + count));
        }
        
        insert accounts;
        System.debug('accounts : ' + accounts);
        String recordTypeId = '';
        //Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('DealerTrackMonthlyLoans').getRecordTypeId();
        List<TmpLoadPerformance__c> loadPerformances = new List<TmpLoadPerformance__c>();
        for( Integer count = 0; count < 200; count++ ) {
            loadPerformances.add(buildLoadPerfomance('168' + count,'test',Date.valueOf('2016-10-1'),'book','abc cde','abc cde',recordTypeId, '201630'));    
        }
        for( Integer count = 0; count < 200; count++ ) {
            loadPerformances.add(buildLoadPerfomance('198' + count,'test',Date.valueOf('2016-10-1'),'book','abc cde','abc cde',recordTypeId, '201630'));    
        }
        loadPerformances.add(buildLoadPerfomance('lkl','test account',Date.valueOf('2016-10-1'),'book','cde cde','cde cde',recordTypeId, '20132'));    
        System.debug('loadPerformances in test: ' + loadPerformances);
        Test.startTest();
        //insert loadPerformances;
        Database.insert(loadPerformances, false);
        Test.stopTest();
        
        Integer insertedAccountsCount = [ SELECT COUNT() FROM Account ];
        Integer insertedContactsCount = [ SELECT COUNT() FROM Contact ];
        Integer insertedOpportunitiesCount = [ SELECT COUNT() FROM Opportunity ];
        
        System.assertEquals(insertedAccountsCount, 401);
        System.assertEquals(insertedContactsCount, 0);
        System.assertEquals(insertedOpportunitiesCount, 401);    
    }
    
    static TmpLoadPerformance__c buildLoadPerfomance( String autoWorxNo,
                                String accountName,
                                Date closeDate,
                                String stageName,
                                String contactFirstName,
                                String contactLastName,
                                String recordTypeId,
                                String MonthId
                                ) {
        
        TmpLoadPerformance__c loadPerformance = new TmpLoadPerformance__c();
        loadPerformance.Tmp_Autoworx_no__c = autoWorxNo;
        loadPerformance.Tmp_Account_Name__c = accountName;
        loadPerformance.Tmp_Close_Date__c = closeDate;
        loadPerformance.Tmp_Stage__c = stageName;
        loadPerformance.Tmp_Contact_first_name__c = contactFirstName;
        loadPerformance.Tmp_Contact_last_name__c = contactLastName;
        
        //--For DealerTrackMonthlyLoans
        loadPerformance.Tmp_Month_ID__c = monthId;
        loadPerformance.Tmp_First_Submits__c = 10;
        loadPerformance.Tmp_DESJ_approved_all__c = 20;
        loadPerformance.Tmp_DESJ_declined_all__c = 30;
        loadPerformance.Tmp_DESJ_Look_Non_subvented__c = 40;
        loadPerformance.Tmp_Dealer_Book_Prime_Non_subvented__c = 50;
        loadPerformance.Tmp_DESJ_Approved_non_subvented__c = 60;
        loadPerformance.Tmp_DESJ_Declined_non_subvented__c = 70;
        
        if( String.isBlank(MonthId) ) {
            loadPerformance.Tmp_Opportunity_Record_Type__c = recordTypeId.substring(0, 15);
        }
        return loadPerformance;
    }
    
    static Account buildAccount(String name, String autoWorksNo, String prevAutoWorksNo) {
        Account account = new Account();
        account.Name = name;
        account.Autoworx_no__c = autoWorksNo;
        account.Autoworx_prev_no__c = prevAutoWorksNo;
        return account;
    }
}