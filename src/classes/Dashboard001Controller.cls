/****************************************************************************
 * @purpose: Controller for trading chart Visualforce Page
 * @Created Date: 19/3/16
 ***************************************************************************/
public class Dashboard001Controller {

    public OpportunityWrapper opportunityCurrentYear{get;set;}
    public OpportunityWrapper opportunityLastYear{get;set;}
    public Integer selectedMonth {get; set;}
    public Integer selectedYear {get; set;}
    public String selectedRecordType{get;set;} 
    public String selectedRecordTypeLabel{get;set;}     
    public String currentAccountId;

    public Dashboard001Controller(ApexPages.StandardController controller){

        currentAccountId  = ApexPages.currentPage().getParameters().get('id');
        selectedMonth = Date.today().month();
        selectedYear = Date.today().Year();        
        selectedRecordType='DealerTrackMonthlyOpp';   
        selectedRecordTypeLabel = 'DealerTrackMonthlyLoans';    
        getOpportunity();
    }
    
    /**********************************************************************************************
    * @purpose: To get Opportunity for the selected month-year, which is matching with closeDate
    ***********************************************************************************************/   
    public void getOpportunity(){

        Date selectedDate = selectedMonthYear(); 
        opportunityCurrentYear = queryOnOpportunity(selectedDate);   
        opportunityLastYear = queryOnOpportunity(Date.newInstance(selectedDate.Year()-1 , selectedDate.month(), 1));
        
    }
    
    /**********************************************************************************************
    * @purpose: To query on Opportunity object and get the opportunity for selected month and year
    * @return: opportunity
    ***********************************************************************************************/        
    public OpportunityWrapper queryOnOpportunity(Date fromDate){
         
        Date toDate = Date.newInstance(fromDate.year(), fromDate.month(), Date.daysInMonth(fromDate.year(), fromDate.month())); 
        List<RecordType> recordType = [Select Id FROM RecordType WHERE DeveloperName = :selectedRecordType];                  
        opportunityWrapper opptyResult = new opportunityWrapper();
        
        if(recordType.size() == 1){           
            try{
                List<Opportunity> opportunityList = [SELECT Name, CloseDate, Bookto_Look__c, Prime_Book_to_Look__c, 
                                                            Approval_Percentage__c, Prime_Approval_Percentage__c,
                                                            Book_to_Approval__c, Prime_Book_to_Approval__c, DESJ_book_all__c,
                                                            DESJ_approved_all__c
                                                     FROM Opportunity 
                                                     WHERE RecordTypeId =: recordType[0].Id AND 
                                                           Account.Id = :currentAccountId AND CloseDate >= :fromDate AND
                                                           CloseDate <= :toDate];
                 if(!opportunityList.isEmpty()){
                    opptyResult = getsummarizeDataOfOpportunities(opportunityList);
                 }
            }
            catch(Exception exp){                              
                System.debug('Exception: ' + exp.getMessage());
                 showErrorMessage(ApexPages.Severity.ERROR, 'Exception in Opportunity::'+exp.getMessage());
            }         
        }  
        else{
            System.debug('No recordtype found');
            showErrorMessage(ApexPages.Severity.ERROR, selectedRecordTypeLabel +' Record Type could not be found.' );
        }     
        return opptyResult;
    }
      
      
    /*********************************************************************************************
    * @purpose:To show error messages to user - all to be renamed Dashboard001Controller
    *********************************************************************************************/           
    public void showErrorMessage(ApexPages.Severity severity, String errorMessage){        
        ApexPages.addMessage(new ApexPages.Message(severity, errorMessage));        
    }
               
    /****************************************************************************
    * @purpose: To get all the record types from custom setting
    * @return: list of the recordtypes fetched from custom setting
    ***************************************************************************/    
   public List<TradingChartRecordTypes__c> getRecordTypes(){  
   
        List<TradingChartRecordTypes__c> recordTypeNames = new List<TradingChartRecordTypes__c>();   
        
        for (TradingChartRecordTypes__c recordTypeName : TradingChartRecordTypes__c.getAll().values()){
            recordTypeNames.add(new TradingChartRecordTypes__c(Name = recordTypeName.Name, Label__c = recordTypeName.Label__c));
        }
        return recordTypeNames;     
    } 
    
    /*********************************************************************************************
    * @purpose: To set the date to the 1st of month & year selected by user on Trading chart page
    * @return:  Date             
    *********************************************************************************************/       
    public Date selectedMonthYear(){    
        return Date.newInstance(selectedYear, selectedMonth, 1);        
    }
    
    public class OpportunityWrapper{
        public Decimal booktoLook {get;set;}
        public Decimal approvalToLook {get;set;}
        public Decimal bookToApproval {get;set;}
        public Decimal primeBookToLook {get;set;}
        public Decimal primeApprovalToLook {get;set;}
        public Decimal primeBookToApproval {get;set;}
        
        public opportunityWrapper(){
            booktoLook = 0;
            approvalToLook = 0;
            bookToApproval = 0;
            primeBookToLook = 0;
            primeApprovalToLook = 0;
            primeBookToApproval = 0;
        }
    }
    
    private OpportunityWrapper getsummarizeDataOfOpportunities(List<Opportunity> opportunityList){
        
        opportunityWrapper opptyResult = new opportunityWrapper();
        
        for(Opportunity oppty : opportunityList){
                                                    
            opptyResult.booktoLook += oppty.Bookto_Look__c == null? 0: oppty.Bookto_Look__c;
            opptyResult.primeBookToLook += oppty.Prime_Book_to_Look__c == null ? 0 :oppty.Prime_Book_to_Look__c;
            opptyResult.approvalToLook += oppty.Approval_Percentage__c == null ? 0 :oppty.Approval_Percentage__c ;
            opptyResult.primeApprovalToLook += oppty.Prime_Approval_Percentage__c == null?0:oppty.Prime_Approval_Percentage__c;
            opptyResult.bookToApproval += oppty.Book_to_Approval__c == null ? 0 : oppty.Book_to_Approval__c ;
            opptyResult.primeBookToApproval += oppty.Prime_Book_to_Approval__c == null ? 0 :oppty.Prime_Book_to_Approval__c;
        
        }
        
        if(!opportunityList.isEmpty()){
        
            Integer listSize = opportunityList.size();
            
            opptyResult.booktoLook = opptyResult.booktoLook / listSize;
            opptyResult.primeBookToLook = opptyResult.primeBookToLook / listSize;
            opptyResult.approvalToLook = opptyResult.approvalToLook / listSize;
            opptyResult.primeApprovalToLook = opptyResult.primeApprovalToLook / listSize;
            opptyResult.bookToApproval = opptyResult.bookToApproval / listSize;
            opptyResult.primeBookToApproval = opptyResult.primeBookToApproval / listSize;
        }
        
        return opptyResult;
    }
}