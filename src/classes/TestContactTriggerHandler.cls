/****************************************************************************
 * @purpose: Test Class for ContactTriggerHandler Class
 * @Created Date: 25/3/16
 ***************************************************************************/
@isTest(SeeAllData=true)
public class TestContactTriggerHandler {
    
    /****************************************************************************
    * @purpose: Insert City Records and Create its Map
    * @retrun: Map of<Id, City__c>
    ***************************************************************************/    
    public static Map<Id,City__c> insertCities(){
        Map<Id,City__c> cityMap = new Map<Id,City__c>();        
        List<City__c> cityList = new List<City__c>();        
        for(Integer i = 0; i < 200; i++){
            cityList.add(new City__c(Name = 'City ' + i, Province_Code__c = 'PC ' + i));    
        }       
        insert cityList;
        cityMap.putAll(cityList);
        return cityMap;
    }
    
    
    /****************************************************************************
    * @purpose: Insert Contact records
    * @ param:  List<City__c>
    * @retrun:  List of Contact to be inserted
    ***************************************************************************/    
    public static List<Contact> insertContact(List<City__c> cityList){
        
        List<Contact> contactList = new List<Contact>();  
        System.debug('cityList Size in inser ACCOUNT: ' + cityList.size());
        
        for(Integer count = 0; count < 180; count++){
            System.debug('Count VAl: ' + count +'-->'+  cityList[count].Name);
            contactList.add(new Contact(LastName = 'Contact' + count, City__c = cityList[count].Id));
        }
        for(Integer count = 180; count < 200; count++){
            contactList.add(new Contact(LastName = 'Contact' + count, City__c = null, 
                                        MailingCity ='Venice'));            
        }                     
        
        return contactList;
    }
    
    
    /****************************************************************************
    * @purpose: Update Contact Records
    * @ param:  List of Contact to be Updated and List of Citites
    * @retrun:  List of Contact to be updated
    ***************************************************************************/
    public static List<Contact> updateContact(List<Contact> contactListToUpdate,List<City__c> cityList){
        Integer count;
        for(count = 0; count < 75 ; count++){                       
            contactListToUpdate[count].City__c = null; 
            contactListToUpdate[count].MailingCity ='Berlin';
        }                     
                
        for(count = 75; count < 180 ; count++){
            contactListToUpdate[count].City__c = cityList[count - 2].Id;            
        }          
        return contactListToUpdate;
    }
        
    /***********************************************************************************************
    * @purpose: Chceck if Test case is running properly, with Assert Statements
    * @ param:  Array of Database.SaveResult,List of Contact to be Updated/Inserted and Map of City 
    ************************************************************************************************/
    public static void performAssertion(Database.SaveResult[] saveResultList, List<Contact> contactList,Map<Id,City__c> cityMap){
        
        for(Contact contact :contactList){            
            if(contact.City__c == null){
                System.assert(true,String.isEmpty(contact.MailingCity));                
            }
            else{                
                System.assertEquals(contact.MailingCity, cityMap.get(contact.City__c).Name);                
            }
        }
        
        for(Database.SaveResult ds: saveResultList){            
            if(!ds.isSuccess()){                                
                for(Database.Error error: ds.getErrors()){                      
                    if(error.getFields()[0] == 'MailingCity'){
                        System.assertEquals('User should add Mailing City Value to City field', error.getMessage());    
                    } 
                }                
            }
        }
    }
        
    /***********************************************************************************************
    * @purpose: Test method to perform test of PopulateStandardCityAndProvinceCode() function   
    ************************************************************************************************/
    static testMethod void testPopulateStandardCityAndProvinceCode(){
        try{
        Map<Id,City__c> cityMap = insertCities();        
        List<Contact> contactList = insertContact(cityMap.values());         

        Test.startTest();        
        
        Database.SaveResult[] insertResult = Database.insert(contactList,false);   
        List<Contact> insertedContactList = [SELECT Name, City__c, MailingCity FROM Contact];
        performAssertion(insertResult,insertedContactList,cityMap);
        
        List<Contact> contactToUpdateList = updateContact(insertedContactList, cityMap.values());
        Database.SaveResult[] updateResult = Database.update(contactToUpdateList,false);
        List<Contact> updatedContactList = [SELECT Name, City__c, MailingCity FROM Contact];
        performAssertion(updateResult,updatedContactList,cityMap);
        
        Test.stopTest();     
        }
        catch(Exception e){
        }     
    }    
}