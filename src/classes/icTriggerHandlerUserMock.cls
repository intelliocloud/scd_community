@isTest
public class icTriggerHandlerUserMock implements icTriggerHandlerUser.IClass{

	public void onBeforeInsert(List<User> newUsers, Map<Id, User> userMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeInsert');
		params.put('newUsers', newUsers);
		params.put('userMap', userMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeInsert');
	}

	public void onAfterInsert(List<User> oldUser, List<User> newUsers, Map<Id, User> userMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterInsert');
		params.put('oldUser', oldUser);
		params.put('newUsers', newUsers);
		params.put('userMap', userMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterInsert');
	}

	public void onBeforeUpdate(List<User> oldUser, List<User> newUsers, Map<Id, User> userMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
		params.put('oldUser', oldUser);
		params.put('newUsers', newUsers);
		params.put('userMap', userMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeUpdate');
	}

	public void onAfterUpdate(List<User> oldUser, List<User> newUsers, Map<Id, User> userMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterUpdate');
		params.put('oldUser', oldUser);
		params.put('newUsers', newUsers);
		params.put('userMap', userMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterUpdate');
	}
}