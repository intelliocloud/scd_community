@isTest
public class icRepositoryUserMock implements icRepositoryUser.IClass{

	public User getUserInfoById(String userId) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getUserInfoById');
		params.put('userId', userId);
		Return (User) icTestMockUtilities.Tracer.GetReturnValue(this, 'getUserInfoById');
	}

	public List<User> getPartnerUsersByContactIds(Set<String> contactIds) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerUsersByContactIds');
		params.put('contactIds', contactIds);
		Return (List<User>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerUsersByContactIds');
	}

	public List<Profile> getPartnerProfile() {
		//Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerProfile');
		//params.put('parameter', parameter);
		Return (List<Profile>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerProfile');
	}

	public List<UserRole> getPartnerUserRolesByAccountIds(Set<String> accountIds){
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerUserRolesByAccountIds');
		params.put('accountIds', accountIds);
		Return (List<UserRole>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerUserRolesByAccountIds');
	}

	public void createUsers(List<User> newUsers) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'createUsers');
		params.put('newUsers', newUsers);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'createUsers');
	}

	public void editUsers(List<User> users) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'editUsers');
		params.put('users', users);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'editUsers');
	}
}