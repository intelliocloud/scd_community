public class icBusinessLogicOpportunity implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		Opportunity generateOpportunityForLead(Lead thisLead, String opportunityName);
		void createOpportunities(List<Opportunity> newOpportunities);
	}

	public class Impl implements IClass {

		icRepositoryOpportunity.IClass repository = (icRepositoryOpportunity.IClass) icObjectFactory.GetSingletonInstance('icRepositoryOpportunity');

		public Opportunity generateOpportunityForLead(Lead thisLead, String opportunityName) {
			List<RecordType> opportunityRecordTypes = repository.getRecordTypes();
			RecordType thisRecordType = opportunityRecordTypes[0];
			for(RecordType thisRT : opportunityRecordTypes) {
				if(thisRT.DeveloperName == 'eSpace_Auto') {
					thisRecordType = thisRT;
					break;
				}
			}

			String generatedName = thisRecordType.Name + ' ' + opportunityName + ' : ' + thisLead.FirstName + ' ' + thisLead.LastName;

			return new Opportunity(RecordTypeId = thisRecordType.Id, Name = generatedName, AccountId = thisLead.Dealer_ID__c, StageName = 'Book', CloseDate = Date.today());
		}

		public void createOpportunities(List<Opportunity> newOpportunities) {
			repository.createOpportunities(newOpportunities);
		}
	}
}