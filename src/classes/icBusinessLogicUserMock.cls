@isTest
public class icBusinessLogicUserMock implements icBusinessLogicUser.IClass{

	public User getUserInfoById(String userId) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getUserInfoById');
		params.put('userId', userId);
		Return (User) icTestMockUtilities.Tracer.GetReturnValue(this, 'getUserInfoById');
	}

	public List<User> getPartnerUsersByContactIds(Set<String> contactIds) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerUsersByContactIds');
		params.put('contactIds', contactIds);
		Return (List<User>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerUsersByContactIds');
	}

	public Map<String, User> getMapPartnerUserByContactId(List<User> users) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMapPartnerUserByContactId');
		params.put('users', users);
		Return (Map<String, User>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMapPartnerUserByContactId');
	}

	public List<Profile> getPartnerProfile() {
		//Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerUsersByContactIds');
		//params.put('attribute', attribute);
		return (List<Profile>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerProfile');
	}

	public Map<String, Profile> getMapProfileByName(List<Profile> profiles) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMapProfileByName');
		params.put('profiles', profiles);
		Return (Map<String, Profile>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMapProfileByName');
	}

	public List<UserRole> getPartnerUserRolesByAccountIds(Set<String> accountIds) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerUserRolesByAccountIds');
		params.put('accountIds', accountIds);
		Return (List<UserRole>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerUserRolesByAccountIds');
	}

	public Map<String, List<UserRole>> getMapPartnerUserRolesByAccountId(List<UserRole> roles) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMapPartnerUserRolesByAccountId');
		params.put('roles', roles);
		Return (Map<String, List<UserRole>>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMapPartnerUserRolesByAccountId');
	}

	public String generateUserAliasFromName(String firstName, String lastName) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'generateUserAliasFromName');
		params.put('firstName', firstName);
		params.put('lastName', lastName);
		Return (String) icTestMockUtilities.Tracer.GetReturnValue(this, 'generateUserAliasFromName');
	}

	public void createUsers(List<User> newUsers) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'createUsers');
		params.put('newUsers', newUsers);
		//Return (returnType) icTestMockUtilities.Tracer.GetReturnValue(this, 'createUsers');
	}

	public void editUsers(List<User> users) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'editUsers');
		params.put('users', users);
		//Return (returnType) icTestMockUtilities.Tracer.GetReturnValue(this, 'editUsers');
	}
}