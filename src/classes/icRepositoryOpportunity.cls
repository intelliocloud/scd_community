public class icRepositoryOpportunity implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<RecordType> getRecordTypes();
		void createOpportunities(List<Opportunity> newOpportunities);
	}

	public class Impl implements IClass {

		public List<RecordType> getRecordTypes() {
			return [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType = 'Opportunity'];
		}

		public void createOpportunities(List<Opportunity> newOpportunities) {
			insert newOpportunities;
		}
	}
}