@isTest
public class icBusinessLogicContactMock implements icBusinessLogicContact.IClass{

	public Contact getContactInfoById(String contactId) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getContactInfoById');
		params.put('contactId', contactId);
		Return (Contact) icTestMockUtilities.Tracer.GetReturnValue(this, 'getContactInfoById');
	}

	public List<Contact> getContactsByDealerNames(Set<String> dealerNames) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getContactsByDealerNames');
		params.put('dealerNames', dealerNames);
		Return (List<Contact>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getContactsByDealerNames');
	}

	public Map<String, List<Contact>> getMapContactsByDealer(List<Contact> contacts) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMapContactsByDealer');
		params.put('contacts', contacts);
		Return (Map<String, List<Contact>>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMapContactsByDealer');
	}

	public List<Contact> getManagersFromContacts(List<Contact> contacts, List<Configuration_Label__mdt> managerContactRoles) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getManagersFromContacts');
		params.put('contacts', contacts);
		params.put('managerContactRoles', managerContactRoles);
		Return (List<Contact>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getManagersFromContacts');
	}
}