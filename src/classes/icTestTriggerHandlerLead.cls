@isTest
public with sharing class icTestTriggerHandlerLead {

	static void initTest() {		
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icBusinessLogicUser', new icBusinessLogicUserMock());
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icBusinessLogicAccount', new icBusinessLogicAccountMock());
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icBusinessLogicOpportunity', new icBusinessLogicOpportunityMock());
	}

	static testMethod void test_onBeforeInsert() {
		initTest();

		String thisLeadId = icTestHelperUtility.getFakeId(Lead.SObjectType);
		String thisContactId = icTestHelperUtility.getFakeId(Contact.SObjectType);
		String thisAccountId = icTestHelperUtility.getFakeId(Account.SObjectType);

		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'getPartnerUsersByContactIds', new List<User>());

		Map<String, User> mockMapPartnerUsersByContactId = new Map<String, User>();
		User mockPartnerUser = new User(
			Username = 'username@domain.com',
			ContactId = thisContactId,			
			Alias = 'uidAlias',
			Email = 'email@domain.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'Fisrt',
			LastName = 'Last',
			CommunityNickname = 'uidAlias',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US');
		mockMapPartnerUsersByContactId.put(thisContactId, mockPartnerUser);
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicUserMock', 'getMapPartnerUserByContactId', mockMapPartnerUsersByContactId);

		Lead thisLead = new Lead(Id = thisLeadId
			,FirstName = 'First'
			,LastName = 'Last'
			,Company = 'Company'
			,Contact_ID__c = thisContactId
			,Dealer_ID__c = thisAccountId);

		List<Lead> oldLeads = new List<Lead>();
		oldLeads.add(thisLead);
		List<Lead> newLeads = new List<Lead>();
		newLeads.add(thisLead);
		Map<Id, Lead> leadMap = new Map<Id, Lead>();
		leadMap.put(thisLeadId, thisLead);
		
		icTriggerHandlerLead.IClass handler = (icTriggerHandlerLead.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerLead');
		handler.onBeforeInsert(newLeads, leadMap);
	}

	static testMethod void test_onAfterInsert() {
		List<Lead> oldLeads = new List<Lead>();
		List<Lead> newLeads = new List<Lead>();
		Map<Id, Lead> leadMap = new Map<Id, Lead>();

		icTriggerHandlerLead.IClass handler = (icTriggerHandlerLead.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerLead');
		handler.onAfterInsert(oldLeads, newLeads, leadMap);
	}

	static testMethod void test_onBeforeUpdate() {
		List<Lead> oldLeads = new List<Lead>();
		List<Lead> newLeads = new List<Lead>();
		Map<Id, Lead> leadMap = new Map<Id, Lead>();
		
		icTriggerHandlerLead.IClass handler = (icTriggerHandlerLead.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerLead');
		handler.onBeforeUpdate(oldLeads, newLeads, leadMap);
	}

	static testMethod void test_onAfterUpdate() {
		initTest();

		String thisLeadId = icTestHelperUtility.getFakeId(Lead.SObjectType);
		String thisContactId = icTestHelperUtility.getFakeId(Contact.SObjectType);
		String thisAccountId = icTestHelperUtility.getFakeId(Account.SObjectType);

		Account convertAccount = new Account(Name = 'Desjardins');
		insert convertAccount;
		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicAccountMock', 'getAccountByName', convertAccount);

		icTestMockUtilities.Tracer.SetReturnValue('icBusinessLogicOpportunityMock', 'generateOpportunityForLead', new Opportunity());

		Lead thisOldLead = new Lead(Id = thisLeadId
			,FirstName = 'First'
			,LastName = 'Last'
			,Status = 'Open'
			,Company = 'Company'
			,Contact_ID__c = thisContactId
			,Dealer_ID__c = thisAccountId);
		Lead thisNewLead = new Lead(Id = thisLeadId
			,FirstName = 'First'
			,LastName = 'Last'
			,Status = 'Qualified'
			,Company = 'Company'
			,Contact_ID__c = thisContactId
			,Dealer_ID__c = thisAccountId);

		List<Lead> oldLeads = new List<Lead>();
		oldLeads.add(thisOldLead);
		List<Lead> newLeads = new List<Lead>();
		newLeads.add(thisNewLead);
		Map<Id, Lead> leadMap = new Map<Id, Lead>();
		leadMap.put(thisLeadId, thisNewLead);

		icTriggerHandlerLead.IClass handler = (icTriggerHandlerLead.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerLead');
		handler.onAfterUpdate(oldLeads, newLeads, leadMap);
	}

	static testMethod void test_onBeforeDelete() {
		List<Lead> oldLeads = new List<Lead>();
		List<Lead> newLeads = new List<Lead>();
		Map<Id, Lead> leadMap = new Map<Id, Lead>();
		
		icTriggerHandlerLead.IClass handler = (icTriggerHandlerLead.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerLead');
		handler.onBeforeDelete(oldLeads, leadMap);
	}

	static testMethod void test_onAfterDelete() {
		List<Lead> oldLeads = new List<Lead>();
		List<Lead> newLeads = new List<Lead>();
		Map<Id, Lead> leadMap = new Map<Id, Lead>();
		
		icTriggerHandlerLead.IClass handler = (icTriggerHandlerLead.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerLead');
		handler.onAfterDelete(oldLeads, leadMap);
	}

	static testMethod void test_onAfterUndelete() {
		List<Lead> oldLeads = new List<Lead>();
		List<Lead> newLeads = new List<Lead>();
		Map<Id, Lead> leadMap = new Map<Id, Lead>();
		
		icTriggerHandlerLead.IClass handler = (icTriggerHandlerLead.IClass) icObjectFactory.GetSingletonInstance('icTriggerHandlerLead');
		handler.onAfterUndelete(oldLeads, newLeads, leadMap);
	}
}