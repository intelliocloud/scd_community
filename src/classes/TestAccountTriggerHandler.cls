/****************************************************************************
 * @purpose: Test Class for AccountTriggerHandler Class
 * @Created Date: 17/3/16
 ***************************************************************************/
@isTest
private class TestAccountTriggerHandler {
    
    /****************************************************************************
 	* @purpose: Insert City Records and Create its Map
 	* @retrun: Map of<Id, City__c>
 	***************************************************************************/    
    public static Map<Id,City__c> insertCities(){
        Map<Id,City__c> cityMap = new Map<Id,City__c>();        
        List<City__c> cityList = new List<City__c>();        
        for(Integer i = 0; i < 200; i++){
        	cityList.add(new City__c(Name = 'City ' + i, Province_Code__c = 'PC ' + i));    
        }       
        insert cityList;
		cityMap.putAll(cityList);
        return cityMap;
    }
    
	
    /****************************************************************************
 	* @purpose: Insert Account records
	* @ param:  List<City__c>
 	* @retrun:  List of Account to be inserted
 	***************************************************************************/	
    public static List<Account> insertAccount(List<City__c> cityList){
        
        List<Account> accountList = new List<Account>();  
        System.debug('cityList Size in inser ACCOUNT: ' + cityList.size());
        
        for(Integer count = 0; count < 180; count++){
            System.debug('Count VAl: ' + count +'-->'+  cityList[count].Name);
            accountList.add(new Account(Name = 'Account' + count, City1__c = cityList[count].Id,City2__c = cityList[count].Id));
        }
        for(Integer count = 180; count < 190; count++){
            accountList.add(new Account(Name = 'Account' + count, City1__c = cityList[count].Id, 
                                        City2__c = null, BillingCity ='Venice'));            
        }       
        
        for(Integer count = 190; count < 200; count++){
            accountList.add(new Account(Name = 'Account' + count, City2__c = cityList[count].Id,
                                        City1__c = null, ShippingCity ='London'));            
        }         
        
        return accountList;
    }
    
    
    /****************************************************************************
 	* @purpose: Update Account Records
	* @ param:  List of Account to be Updated and List of Citites
 	* @retrun:  List of Account to be updated
 	***************************************************************************/
    public static List<Account> updateAccount(List<Account> accountListToUpdate,List<City__c> cityList){
		Integer count;
        for(count = 0; count < 75 ; count++){
        	accountListToUpdate[count].City2__c = cityList[count + 2].Id;            
            accountListToUpdate[count].City1__c = null; 
            accountListToUpdate[count].ShippingCity ='Berlin';
        }
               
        for(count = 76; count < 150; count++){
        	accountListToUpdate[count].City1__c = cityList[count + 2].Id;
            accountListToUpdate[count].City2__c = null;                         
            accountListToUpdate[count].BillingCity ='Paris';            
        }        
                
        for(count = 151; count < 180 ; count++){
			accountListToUpdate[count].City1__c = cityList[count - 2].Id;
            accountListToUpdate[count].City2__c = cityList[count - 2].Id;
        }          
        return accountListToUpdate;
    }
        
    /***********************************************************************************************
 	* @purpose: Chceck if Test case is running properly, with Assert Statements
	* @ param:  Array of Database.SaveResult,List of Account to be Updated/Inserted and Map of City 
 	************************************************************************************************/
    public static void performAssertion(Database.SaveResult[] saveResultList, List<Account> accountList,Map<Id,City__c> cityMap){
        Integer count=0;
        for(Account account :accountList){            
            if(account.City1__c == null){
                System.assert(true,String.isEmpty(account.ShippingCity));                
            }
            else{
                System.debug('count1111 insertedaccountlist: ' + count +'**'+ cityMap.get(account.City1__c).Name +'**'+account.ShippingCity);    
                System.assertEquals(account.ShippingCity, cityMap.get(account.City1__c).Name);                
            }
            
            if(account.City2__c == null){
                System.assert(true,String.isEmpty(account.BillingCity));
            }
            else{
                System.debug('count2222 insertedaccountlist: ' + count +'**'+ cityMap.get(account.City2__c).Name +'**'+account.BillingCity);                    
                System.assertEquals(account.BillingCity, cityMap.get(account.City2__c).Name);
            } 
            count++;
        }
        
        for(Database.SaveResult ds: saveResultList){        	
            if(!ds.isSuccess()){                                
                for(Database.Error error: ds.getErrors()){                      
                    if(error.getFields()[0] == 'BillingCity'){
                        System.assertEquals('User should add Billing city value to City2 field', error.getMessage());    
                    }
                    else if( error.getFields()[0] == 'ShippingCity'){
                        System.assertEquals('User should add Shipping city value to City1 field', error.getMessage());                        
                    } 
                }                
            }
        }
    }
        
    /***********************************************************************************************
 	* @purpose: Test method to perform test of PopulateStandardCityAndProvinceCode() function	
 	************************************************************************************************/
    static testMethod void testPopulateStandardCityAndProvinceCode(){
        
		Map<Id,City__c> cityMap = insertCities();        
        List<Account> accountList = insertAccount(cityMap.values());         

        Test.startTest();        
        
        Database.SaveResult[] insertResult = Database.insert(accountList,false);   
        List<Account> insertedAccountList = [SELECT Name,City1__c, City2__c, ShippingCity, BillingCity FROM Account];
		performAssertion(insertResult,insertedAccountList,cityMap);
        
        List<Account> accountToUpdateList = updateAccount(insertedAccountList, cityMap.values());
        Database.SaveResult[] updateResult = Database.update(accountToUpdateList,false);
        List<Account> updatedAccountList = [SELECT Name,City1__c, City2__c, ShippingCity, BillingCity FROM Account];
        performAssertion(updateResult,updatedAccountList,cityMap);
        
        Test.stopTest();          

    }    
}