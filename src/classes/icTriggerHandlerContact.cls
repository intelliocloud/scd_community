public class icTriggerHandlerContact implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		void onBeforeInsert(List<Contact> newContacts, Map<Id, Contact> contactMap);
		void onAfterInsert(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap);
		void onBeforeUpdate(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap);
		void onAfterUpdate(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap);
		void onBeforeDelete(List<Contact> oldContacts, Map<Id, Contact> contactMap);
		void onAfterDelete(List<Contact> oldContacts, Map<Id, Contact> contactMap);
		void onAfterUndelete(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap);
	}

	public class Impl implements IClass {

		icBusinessLogicMetadata.IClass blMetadata = (icBusinessLogicMetadata.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicMetadata');
		icBusinessLogicAccount.IClass blAccount = (icBusinessLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAccount');		
		icBusinessLogicUser.IClass blUser = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');

		public void onBeforeInsert(List<Contact> newContacts, Map<Id, Contact> contactMap) {
		}

		public void onAfterInsert(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap) {
			Set<String> setAccountIds = new Set<String>();

			List<Partner_User_Configuration__mdt> partnerUserConfigurations = blMetadata.getPartnerUserConfigurations();
			Map<String, Partner_User_Configuration__mdt> mapPartnerConfigByTitleSegmentation = blMetadata.getPartnerUserConfigurations(partnerUserConfigurations);

			for(Contact thisContact : newContacts) {
				setAccountIds.add(thisContact.AccountId);
			}

			List<Account> allPartnerAccounts =  blAccount.getPartnerAccountsFromList(setAccountIds);
			Map<String, Account> mapAccountById = blAccount.getMapAccountById(allPartnerAccounts);

			List<Profile> partnerProfiles = blUser.getPartnerProfile();
			Map<String, Profile> mapPartnerProfilesByName = blUser.getMapProfileByName(partnerProfiles);

			List<User> newPartnerUsers = new List<User>();
			Map<Contact, User> mapPartnerUserByContact = new Map<Contact, User>();
			Map<String, Partner_User_Configuration__mdt> mapPartnerUserConfigByContactId = new Map<String, Partner_User_Configuration__mdt>();

			for(Contact thisContact : newContacts) {
				Account thisContactPartnerAccount = mapAccountById.get(thisContact.AccountId);
				if(thisContactPartnerAccount != null) {

					Partner_User_Configuration__mdt partnerUserConfiguration = mapPartnerConfigByTitleSegmentation.get(thisContact.Contact_title__c + '_' + thisContact.Related_Segment__c);
					if(partnerUserConfiguration == null) {
						partnerUserConfiguration = mapPartnerConfigByTitleSegmentation.get('Default_Default');
					}
					mapPartnerUserConfigByContactId.put(thisContact.Id, partnerUserConfiguration);

					Profile thisPartnerUserProfile = mapPartnerProfilesByName.get(partnerUserConfiguration.Partner_Profile__c);
					String thisUserAlias = blUser.generateUserAliasFromName(thisContact.FirstName, thisContact.LastName);

					String languageLocalKey = 'fr';
					String languageSidKey = 'fr_CA';
					if(String.isNotBlank(thisContact.Related_language__c) && 
						(thisContact.Related_language__c == 'E' ||
						thisContact.Related_language__c == 'English' ||
						thisContact.Related_language__c == 'A' ||
						thisContact.Related_language__c == 'Anglais')
					){
						languageLocalKey = 'en_US';
						languageSidKey = 'en_CA';
					}
					

					User newPartnerUser = new User(
						Username = thisContact.Email + '.partner',
						ContactId = thisContact.Id,
						ProfileId = thisPartnerUserProfile.Id,
						Alias = thisUserAlias,
						Email = thisContact.Email,
						EmailEncodingKey = 'UTF-8',
						FirstName = thisContact.FirstName,
						LastName = thisContact.LastName,
						CommunityNickname = thisUserAlias,
						TimeZoneSidKey = 'America/Los_Angeles',
						LocaleSidKey = languageSidKey,
						LanguageLocaleKey = languageLocalKey);

					newPartnerUsers.add(newPartnerUser);
					mapPartnerUserByContact.put(thisContact, newPartnerUser);
				}
			}
			if(newPartnerUsers.size() > 0) {
				System.debug('do insert here : ' + newPartnerUsers.size());
				blUser.createUsers(newPartnerUsers);
			}

			List<UserRole> allPartnerAccountsRoles = blUser.getPartnerUserRolesByAccountIds(setAccountIds);
			Map<String, List<UserRole>> mapUserRolesByAccountId = blUser.getMapPartnerUserRolesByAccountId(allPartnerAccountsRoles);

			Map<Id, Id> mapUserRoleIdByUserId = new Map<Id, Id>();
			for(Contact partnerContact : mapPartnerUserByContact.keySet()) {
				Account thisContactPartnerAccount = mapAccountById.get(partnerContact.AccountId);
				List<UserRole> partnerUserRoles = mapUserRolesByAccountId.get(thisContactPartnerAccount.Id);

				Partner_User_Configuration__mdt thisPartnerUserConfig = mapPartnerUserConfigByContactId.get(partnerContact.Id);

				UserRole thisUserRole;
				for(UserRole thisRole : partnerUserRoles) {
					if(thisRole.Name.indexOf(thisPartnerUserConfig.Partner_Role__c) != -1) {
						thisUserRole = thisRole;
					}
				}				
				 
				User thisContactPartnerUser = mapPartnerUserByContact.get(partnerContact);

				System.debug('updatePartnerUserRole(' + thisContactPartnerUser.Id + ', ' + thisUserRole.Id + ')');

				mapUserRoleIdByUserId.put(thisContactPartnerUser.Id, thisUserRole.Id);				
			}
			if(!mapUserRoleIdByUserId.isEmpty() && !Test.isRunningTest()) {
				icFutureHelper.updatePartnerUserRole(mapUserRoleIdByUserId);
			}
		}

		public void onBeforeUpdate(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap) {
		}

		public void onAfterUpdate(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap) {
		}

		public void onBeforeDelete(List<Contact> oldContacts, Map<Id, Contact> contactMap) {
		}

		public void onAfterDelete(List<Contact> oldContacts, Map<Id, Contact> contactMap) {
		}

		public void onAfterUndelete(List<Contact> oldContacts, List<Contact> newContacts, Map<Id, Contact> contactMap) {
		}
	}
}