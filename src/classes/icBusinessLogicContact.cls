public class icBusinessLogicContact implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		Contact getContactInfoById(String contactId);
		List<Contact> getContactsByDealerNames(Set<String> dealerNames);
		Map<String, List<Contact>> getMapContactsByDealer(List<Contact> contacts);
		List<Contact> getManagersFromContacts(List<Contact> contacts, List<Configuration_Label__mdt> managerContactRoles);
	}

	public class Impl implements IClass {

		icRepositoryContact.IClass repository = (icRepositoryContact.IClass) icObjectFactory.GetSingletonInstance('icRepositoryContact');

		public Contact getContactInfoById(String contactId) {
			return repository.getContactInfoById(contactId);
		}

		public List<Contact> getContactsByDealerNames(Set<String> dealerNames) {
			return repository.getContactsByDealerNames(dealerNames);
		}

		public Map<String, List<Contact>> getMapContactsByDealer(List<Contact> contacts) {
			Map<String, List<Contact>> mapContactsByDealer = new Map<String, List<Contact>>();

			for(Contact thisContact : contacts) {
				List<Contact> thisDealersContacts = mapContactsByDealer.get(thisContact.Account.Name);
				if(thisDealersContacts == null) {
					thisDealersContacts = new List<Contact>();
				}
				thisDealersContacts.add(thisContact);
				mapContactsByDealer.put(thisContact.Account.Name, thisDealersContacts);
			}

			return mapContactsByDealer;
		}

		public List<Contact> getManagersFromContacts(List<Contact> contacts, List<Configuration_Label__mdt> managerContactRoles) {
			List<Contact> managers = new List<Contact>();

			for(Contact thisContact : contacts) {
				for(Configuration_Label__mdt managerContactRole : managerContactRoles) {
					if(thisContact.Account_Contact_Role__c != null && thisContact.Account_Contact_Role__c.indexOf(managerContactRole.Label_Value__c) != -1) {
						managers.add(thisContact);
					}
				}
			}

			return managers;
		}
	}
}