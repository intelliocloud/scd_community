public class icTriggerHandlerLead implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		void onBeforeInsert(List<Lead> newLeads, Map<Id, Lead> leadMap);
		void onAfterInsert(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap);
		void onBeforeUpdate(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap);
		void onAfterUpdate(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap);
		void onBeforeDelete(List<Lead> oldLeads, Map<Id, Lead> leadMap);
		void onAfterDelete(List<Lead> oldLeads, Map<Id, Lead> leadMap);
		void onAfterUndelete(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap);
	}

	public class Impl implements IClass {

		icBusinessLogicMetadata.IClass blMetadata = (icBusinessLogicMetadata.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicMetadata');
		icBusinessLogicAccount.IClass blAccount = (icBusinessLogicAccount.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicAccount');
		icBusinessLogicContact.IClass blContact = (icBusinessLogicContact.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicContact');
		icBusinessLogicOpportunity.IClass blOpportunity = (icBusinessLogicOpportunity.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicOpportunity');
		icBusinessLogicUser.IClass blUser = (icBusinessLogicUser.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicUser');

		public void onBeforeInsert(List<Lead> newLeads, Map<Id, Lead> leadMap) {
			/* 
			V1.1
				Set<String> setDealerNames = new Set<String>();
				List<Configuration_Label__mdt> managerContactRoles = blMetadata.getConfigurationLabelsByType('ManagerContactRole');

				for(Lead thisLead : newLeads) {
					if(String.isNotBlank(thisLead.Company)) {
						setDealerNames.add(thisLead.Company);
					}
				}

				List<Contact> allDealersContacts =  blContact.getContactsByDealerNames(setDealerNames);
				List<Contact> thisDealerManagerContacts = blContact.getManagersFromContacts(allDealersContacts, managerContactRoles);
				Map<String, List<Contact>> mapContactByDealer = blContact.getMapContactsByDealer(thisDealerManagerContacts);
				
				Set<String> managerContactIds = new Set<String>();
				for(Contact thisContact : thisDealerManagerContacts) {
					managerContactIds.add(thisContact.Id);
				}

				if(managerContactIds.size() > 0) {
					List<User> thisDealersManagerPartnerUsers = blUser.getPartnerUsersByContactIds(managerContactIds);
					Map<String, User> mapPartnerUsersByContactId = blUSer.getMapPartnerUserByContactId(thisDealersManagerPartnerUsers);

					for(Lead thisLead : newLeads) {
						List<Contact> thisDealersContacts = mapContactByDealer.get(thisLead.Company);
						if(thisDealersContacts.size() > 0) {
							User thisLeadManagerUser = mapPartnerUsersByContactId.get(thisDealersContacts[0].Id);
							if(thisLeadManagerUser != null) {
								thisLead.OwnerId = thisLeadManagerUser[0].Id;
							}

						}
					}
				}
			
			V1.0
				for(Lead thisLead : newLeads) {
					List<Contact> thisDealersContacts = mapContactByDealer.get(thisLead.Company);
					if(thisDealersContacts != null) {
						List<Contact> thisDealersManagers = blContact.getManagersFromContacts(thisDealersContacts, managerContactRoles);
						
						Set<String> managerIds = new Set<String>();
						for(Contact thisManager : thisDealersManagers) {
							managerIds.add(thisManager.Id);
						}

						if(managerIds.size() > 0) {
							List<User> thisDealersManagerPartnerUsers = blUser.getPartnerUsersByContactIds(managerIds);
							if(thisDealersManagerPartnerUsers.size() > 0) {
								thisLead.OwnerId = thisDealersManagerPartnerUsers[0].Id;
							}
						}
					}
				}
			*/

			/* V2 */
			Set<String> managerContactIds = new Set<String>();
			for(Lead thisLead : newLeads) {
				managerContactIds.add(thisLead.Contact_ID__c);
				thisLead.Contact__c = thisLead.Contact_ID__c;
				thisLead.Dealer__c = thisLead.Dealer_ID__c;
			}

			System.debug('managerContactIds : ' + managerContactIds);

			if(managerContactIds.size() > 0) {
				List<User> thisDealersManagerPartnerUsers = blUser.getPartnerUsersByContactIds(managerContactIds);
				System.debug('thisDealersManagerPartnerUsers : ' + thisDealersManagerPartnerUsers);
				Map<String, User> mapPartnerUsersByContactId = blUSer.getMapPartnerUserByContactId(thisDealersManagerPartnerUsers);
				System.debug('mapPartnerUsersByContactId : ' + mapPartnerUsersByContactId);

				for(Lead thisLead : newLeads) {
					String caseInsensitiveId = icObjectFactory.convertIdToCaseInsensitive(thisLead.Contact_ID__c.left(15));
					User thisLeadManagerUser = mapPartnerUsersByContactId.get(caseInsensitiveId);
					System.debug('thisLeadManagerUser : ' + thisLeadManagerUser);
					if(thisLeadManagerUser != null) {
						thisLead.OwnerId = thisLeadManagerUser.Id;
					}
				}
			}
		}

		public void onAfterInsert(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
		}

		public void onBeforeUpdate(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
		}

		public void onAfterUpdate(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
			List<Database.LeadConvert> leadsToConvert = new List<Database.LeadConvert>();
			List<Opportunity> newOpportunities = new List<Opportunity>();

			Lead_Conversion_Settings__mdt convertSettings = blMetadata.getAutoConvertLeadSettings();

			Account convertToAccount = blAccount.getAccountByName(convertSettings.Convert_To_Account_Name__c);

			for(Lead thisOldLead : oldLeads) {
				Lead thisNewLead = leadMap.get(thisOldLead.Id);
				if(thisOldLead.Status != convertSettings.Convert_On_Status__c && thisNewLead.Status == convertSettings.Convert_On_Status__c) {
					Database.LeadConvert thisLeadConvert = new Database.LeadConvert();
					thisLeadConvert.setLeadId(thisNewLead.Id);
					thisLeadConvert.setConvertedStatus(thisNewLead.Status);
					thisLeadConvert.setAccountId(convertToAccount.Id);
					thisLeadConvert.setDoNotCreateOpportunity(true);					
					leadsToConvert.add(thisLeadConvert);
					newOpportunities.add(blOpportunity.generateOpportunityForLead(thisNewLead, convertSettings.Converted_Opportunity_Name__c));
				}
			}

			if(!leadsToConvert.isEmpty() && !Test.isRunningTest()) {
				List<Database.LeadConvertResult> leadConvertResults = Database.convertLead(leadsToConvert);				
			}

			if(!newOpportunities.isEmpty()) {
				blOpportunity.createOpportunities(newOpportunities);
			}
		}

		public void onBeforeDelete(List<Lead> oldLeads, Map<Id, Lead> leadMap) {
		}

		public void onAfterDelete(List<Lead> oldLeads, Map<Id, Lead> leadMap) {
		}

		public void onAfterUndelete(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
		}
	}
}