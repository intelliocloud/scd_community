@isTest
public with sharing class icTestBusinessLogicContact {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepositoryContact', new icRepositoryContactMock());
	}

	static testMethod void test_getContactInfoById() {
		initTest();

		Contact testContact = new Contact(LastName = 'Test Contact');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryContactMock', 'getContactInfoById', testContact);

		icBusinessLogicContact.IClass businessLogic = (icBusinessLogicContact.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicContact');
		Contact testResult = businessLogic.getContactInfoById('contactId');
	}

	static testMethod void test_getContactsByDealerNames() {
		initTest();

		Contact testContact = new Contact(LastName = 'Test Contact');
		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryContactMock', 'getContactsByDealerNames', new List<Contact> {testContact});

		icBusinessLogicContact.IClass businessLogic = (icBusinessLogicContact.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicContact');
		List<Contact> testResult = businessLogic.getContactsByDealerNames(new Set<String>());
	}

	static testMethod void test_getMapContactsByDealer() {
		String testDealerNames = 'Test Dealer Name';

		Account testAccount = new Account(
			Name = testDealerNames);
		insert testAccount;

		Contact testContact = new Contact(
			AccountId = testAccount.Id
			,FirstName = 'FirstName'
			,LastName = 'LastName'
			,Email = 'test@fake.com');
		insert testContact;

		List<Contact> testContactInput = [SELECT Id, FirstName, LastName, Title, Account_Contact_Role__c, Account.Name FROM Contact WHERE Account.Name = :testDealerNames];

		icBusinessLogicContact.IClass businessLogic = (icBusinessLogicContact.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicContact');
		Map<String, List<Contact>> testResult = businessLogic.getMapContactsByDealer(testContactInput);
	}

	static testMethod void test_getManagersFromContacts() {		
		Contact testContact = new Contact(LastName = 'Test Contact', Account_Contact_Role__c = 'Lead Manager');

		List<Configuration_Label__mdt> managerContactRoleMeta = [SELECT Id, DeveloperName, Label_Value__c FROM Configuration_Label__mdt WHERE Label_Type__c = 'ManagerContactRole'];

		icBusinessLogicContact.IClass businessLogic = (icBusinessLogicContact.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicContact');
		List<Contact> testResult = businessLogic.getManagersFromContacts(new List<Contact> {testContact}, managerContactRoleMeta);
	}
}