@isTest
public class icRepositoryContactMock implements icRepositoryContact.IClass{

	public Contact getContactInfoById(String contactId) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getContactInfoById');
		params.put('contactId', contactId);
		Return (Contact) icTestMockUtilities.Tracer.GetReturnValue(this, 'getContactInfoById');
	}

	public List<Contact> getContactsByDealerNames(Set<String> dealerNames) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getContactsByDealerNames');
		params.put('dealerNames', dealerNames);
		Return (List<Contact>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getContactsByDealerNames');
	}
}