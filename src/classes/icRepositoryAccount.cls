public class icRepositoryAccount implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Account> getAccountByName(String accountName);
		List<Account> getPartnerAccountsFromList(Set<String> accountIds);
	}

	public class Impl implements IClass {

		public List<Account> getAccountByName(String accountName) {
			return [SELECT Id, Name FROM Account WHERE Name = :accountName];
		}
		
		public List<Account> getPartnerAccountsFromList(Set<String> accountIds) {
			return [SELECT Id, Name FROM Account WHERE Id IN :accountIds AND IsPartner = true];
		}
	}
}