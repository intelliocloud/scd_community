public class icBusinessLogicAccount implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		Account getAccountByName(String accountName);
		List<Account> getPartnerAccountsFromList(Set<String> accountIds);
		Map<String, Account> getMapAccountById(List<Account> accounts);
	}

	public class Impl implements IClass {

		icRepositoryAccount.IClass repository = (icRepositoryAccount.IClass) icObjectFactory.GetSingletonInstance('icRepositoryAccount');

		public Account getAccountByName(String accountName) {
			Account returnAccountByName;
			List<Account> accountsWithName = repository.getAccountByName(accountName);
			if(!accountsWithName.isEmpty()) {
				returnAccountByName = accountsWithName[0];
			}
			return returnAccountByName;
		}

		public List<Account> getPartnerAccountsFromList(Set<String> accountIds) {
			return repository.getPartnerAccountsFromList(accountIds);
		}

		public Map<String, Account> getMapAccountById(List<Account> accounts) {
			Map<String, Account> mapAccountById = new Map<String, Account>();
			for(Account thisAccount : accounts) {
				mapAccountById.put(thisAccount.Id, thisAccount);
			}
			return mapAccountById;
		}
	}
}