/****************************************************************************
 * @purpose: Trigger Handler for Contact Object
 * @Created Date: 17/3/16
 ***************************************************************************/

public class ContactTriggerHandler {
    
  /*******************************************************************
   * @purpose: Function Called Before Insert of Contact Record
   * @param: List of Contact Records
   *******************************************************************/
    public static void isBeforeInsert(List<Contact> contactList){
        
        //Function to validate if city is in list of given Cities
        populateStandardCityAndProvinceCode(contactList);
    }

    
  /*******************************************************************
   * @purpose: Function Called Before Update of Contact Record
   * @param: List of Contact Records
   *******************************************************************/    
    public static void isBeforeUpdate(List<Contact> contacts, Map<Id, Contact> oldContact){
        
        //Function to validate if city is in list of given Cities
        onUpdatePopulateCityAndProvinceCode(contacts, oldContact);
    }
    
    
    /***************************************************************************************************************
    * @purpose: Check whether Contact.MailingCity value changed, if yes repopulate value
    * @param  : a) contacts
    *           b) oldContactMap
    **************************************************************************************************************/
    private static void onUpdatePopulateCityAndProvinceCode(List<Contact> contacts, Map<Id, Contact> oldContactMap) {
        
        List<Contact> contactsToUpdate = new List<Contact>();
        Contact oldContact;
        Boolean isContactCityChanged;
        for(Contact contactRec: contacts) {
            
            if(!contactRec.isUpload__c){
                
                oldContact = oldContactMap.get(contactRec.id);
                isContactCityChanged = false;
                
                if( contactRec.mailingCity != oldContact.mailingCity){                    
                    isContactCityChanged = true;
                }
                                
                if(contactRec.city__c != oldContact.city__c) {
                    
                    isContactCityChanged = true;
                    if(String.isBlank(contactRec.city__c))
                        contactRec.mailingCity = null;
                }                
                if(isContactCityChanged)
                    contactsToUpdate.add(contactRec);
            }            
        }
        
        if(contactsToUpdate.size() > 0)
           populateStandardCityAndProvinceCode(contactsToUpdate);
        
    }
    
   /***************************************************************************************************************
    * @purpose: Populate from 
    * @param  : List of Account Records
    **************************************************************************************************************/     
   private static void populateStandardCityAndProvinceCode(List<Contact> contactList) {
        
        
        Map<Id, City__c> cityMap = getCityMap(contactList);
        City__c cityRec;        
        for(Contact contactRec: contactList) {    
            if(!contactRec.isUpload__c){
                if(String.isNotBlank(contactRec.city__c)){
                    
                    cityRec = cityMap.get(contactRec.city__c);
                    contactRec.mailingCity = cityRec.Name;
                    contactRec.Contact_City_PickList__c = cityRec.Province_Code__c;
                }
                else if(String.isNotBlank(contactRec.mailingCity))
                    contactRec.mailingCity.addError('User should add Mailing City Value to City field');               
            }            
        }        
    }
   
    
    /***************************************************************************************************************
    * @purpose: Fetch City name and province code from City Object.
    * @param  : a) accountList
    * @return : Return map of city object.
    **************************************************************************************************************/ 
   private static Map<Id, City__c> getCityMap(List<Contact> contactList){
        
        Set<Id> cityIdSet = new Set<Id>();   
        for(Contact contactRec: contactList){
            
            if(String.isNotBlank(contactRec.city__C))
                cityIdSet.add(contactRec.city__C);
        }
        
        if(cityIdSet.size() > 0)
            return new Map<Id, City__c>([SELECT Id, Name, Province_Code__c FROM City__c WHERE ID IN: cityIdSet]);
        
        return new Map<Id, City__c>();
    }  
}