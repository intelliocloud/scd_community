@isTest
public class icTriggerHandlerLeadMock implements icTriggerHandlerLead.IClass{

	public void onBeforeInsert(List<Lead> newLeads, Map<Id, Lead> leadMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeInsert');
		params.put('newLeads', newLeads);
		params.put('leadMap', leadMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeInsert');
	}

	public void onAfterInsert(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterInsert');
		params.put('oldLeads', oldLeads);
		params.put('newLeads', newLeads);
		params.put('leadMap', leadMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterInsert');
	}

	public void onBeforeUpdate(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
		params.put('oldLeads', oldLeads);
		params.put('newLeads', newLeads);
		params.put('leadMap', leadMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeUpdate');
	}

	public void onAfterUpdate(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterUpdate');
		params.put('oldLeads', oldLeads);
		params.put('newLeads', newLeads);
		params.put('leadMap', leadMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterUpdate');
	}

	public void onBeforeDelete(List<Lead> oldLeads, Map<Id, Lead> leadMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeDelete');
		params.put('oldLeads', oldLeads);		
		params.put('leadMap', leadMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeDelete');
	}

	public void onAfterDelete(List<Lead> oldLeads, Map<Id, Lead> leadMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterDelete');
		params.put('oldLeads', oldLeads);		
		params.put('leadMap', leadMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterDelete');
	}

	public void onAfterUndelete(List<Lead> oldLeads, List<Lead> newLeads, Map<Id, Lead> leadMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterUndelete');
		params.put('oldLeads', oldLeads);
		params.put('newLeads', newLeads);
		params.put('leadMap', leadMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterUndelete');
	}
}