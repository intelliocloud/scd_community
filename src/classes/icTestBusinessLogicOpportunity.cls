@isTest
public with sharing class icTestBusinessLogicOpportunity {

	static void initTest() {
		icTestMockUtilities.Mocker.SetMockSingletonInstance('icRepositoryOpportunity', new icRepositoryOpportunityMock());
	}

	static testMethod void test_generateOpportunityForLead() {
		initTest();

		Lead newLead = new Lead(Id = icTestHelperUtility.getFakeId(Lead.SObjectType)
			,FirstName = 'First Name'
			,LastName = 'Last Name'
			,Dealer_ID__c = icTestHelperUtility.getFakeId(Account.SObjectType));

		RecordType mockRecordType = new RecordType(Name = 'eSpace Auto'
			,DeveloperName = 'eSpace_Auto');

		icTestMockUtilities.Tracer.SetReturnValue('icRepositoryOpportunityMock', 'getRecordTypes', new List<RecordType> {mockRecordType});

		icBusinessLogicOpportunity.IClass businessLogic = (icBusinessLogicOpportunity.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicOpportunity');
		Opportunity testResult = businessLogic.generateOpportunityForLead(newLead, 'newOpp');
	}

	static testMethod void test_createOpportunities() {
		initTest();

		icBusinessLogicOpportunity.IClass businessLogic = (icBusinessLogicOpportunity.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicOpportunity');
		businessLogic.createOpportunities(new List<Opportunity>());
	}
}