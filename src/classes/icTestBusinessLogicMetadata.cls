@isTest
public with sharing class icTestBusinessLogicMetadata {

	static testMethod void test_getAutoConvertLeadSettings() {
		icBusinessLogicMetadata.IClass businessLogic = (icBusinessLogicMetadata.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicMetadata');
		Lead_Conversion_Settings__mdt testResult = businessLogic.getAutoConvertLeadSettings();
	}

	static testMethod void test_getConfigurationLabelsByType() {
		icBusinessLogicMetadata.IClass businessLogic = (icBusinessLogicMetadata.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicMetadata');
		List<Configuration_Label__mdt> testResult = businessLogic.getConfigurationLabelsByType('ManagerContactRole');
	}

	static testMethod void test_getPartnerUserConfigurations() {
		icBusinessLogicMetadata.IClass businessLogic = (icBusinessLogicMetadata.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicMetadata');
		List<Partner_User_Configuration__mdt> testResult = businessLogic.getPartnerUserConfigurations();
	}

	static testMethod void test_getPartnerUserConfigurations_FromList() {
		icBusinessLogicMetadata.IClass businessLogic = (icBusinessLogicMetadata.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicMetadata');
		List<Partner_User_Configuration__mdt> partnerUserConfigMeta = [SELECT Id, DeveloperName, Contact_Title__c, Related_Segment__c, Partner_Profile__c, Partner_Role__c FROM Partner_User_Configuration__mdt];
		Map<String, Partner_User_Configuration__mdt> testResult = businessLogic.getPartnerUserConfigurations(partnerUserConfigMeta);
	}
}