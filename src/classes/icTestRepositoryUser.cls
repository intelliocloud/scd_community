@isTest
public with sharing class icTestRepositoryUser {

	static testMethod void test_getUserInfoById() {
		icRepositoryUser.IClass repository = (icRepositoryUser.IClass) icObjectFactory.GetSingletonInstance('icRepositoryUser');
		Profile randomProfile = [SELECT Id, Name, UserType FROM Profile WHERE UserType = 'Standard' LIMIT 1];

		User newTestUser = new User(
			Username = 'createUsers@test.mock',	
			ProfileId = randomProfile.Id,
			Alias = 'testmock',
			Email = 'createUsers@test.mock',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'firstName',
			LastName = 'lastName',
			CommunityNickname = 'testmock',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		insert newTestUser;

		User testResult = repository.getUserInfoById(newTestUser.Id);
	}

	static testMethod void test_getPartnerUsersByContactIds() {
		icRepositoryUser.IClass repository = (icRepositoryUser.IClass) icObjectFactory.GetSingletonInstance('icRepositoryUser');

		List<User> testResult = repository.getPartnerUsersByContactIds(new Set<String>());
	}

	static testMethod void test_getPartnerProfile() {
		icRepositoryUser.IClass repository = (icRepositoryUser.IClass) icObjectFactory.GetSingletonInstance('icRepositoryUser');

		List<Profile> testResult = repository.getPartnerProfile();
	}

	static testMethod void test_getPartnerUserRolesByAccountIds() {
		icRepositoryUser.IClass repository = (icRepositoryUser.IClass) icObjectFactory.GetSingletonInstance('icRepositoryUser');

		List<UserRole> testResult = repository.getPartnerUserRolesByAccountIds(new Set<String>());
	}

	static testMethod void test_CRUD() {
		icRepositoryUser.IClass repository = (icRepositoryUser.IClass) icObjectFactory.GetSingletonInstance('icRepositoryUser');

		Profile randomProfile = [SELECT Id, Name, UserType FROM Profile WHERE UserType = 'Standard' LIMIT 1];

		User newTestUser = new User(
			Username = 'createUsers@test.mock',	
			ProfileId = randomProfile.Id,
			Alias = 'testmock',
			Email = 'createUsers@test.mock',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'firstName',
			LastName = 'lastName',
			CommunityNickname = 'testmock',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);

		repository.createUsers(new List<User> {newTestUser});
		repository.editUsers(new List<User> {newTestUser});
	}
}