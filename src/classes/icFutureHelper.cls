global class icFutureHelper
{
	@future
	public static void updatePartnerUserRole(Map<Id, Id> mapUserRoleIdByUserId) {
		List<User> updateUsers = new List<User>();
		for(Id userId : mapUserRoleIdByUserId.keySet()) {
			Id roleId = mapUserRoleIdByUserId.get(userId);
			User updateUser = new User(Id = userId, UserRoleId = roleId);
			updateUsers.add(updateUser);
		}
		if(updateUsers.size() > 0) {
			update updateUsers;
		}
	}
}