/**
  * @Author      : André Thouin
  * @Description : TmpLoadPerformance__c Trigger Handler 
  * @Created Date: 28 Nov 2016
  * @Last Modified date: 22 Dec 2016
                         Purpose - Updated to add section on DealerTrack Performance import
                         10 January 2017
                         Purpose - Updated to add default account and close date if missing in CSV.
 */
public class TmpLoadPerformanceTriggerHandler {
    /**
      * @ Objective: 1) AWX import
                     To assist in loading Monthly AWX data from TmpLoadPerformance and insert into Opportunity AWX records only at this time
                     Monthly import using data loader
                     Set isUpload to default value = True
                     This will then trigger a routine to insert new records in Opportunity
                     If mapping not found using AWX no, THEN look for Previous AWX no
                     If still not found THEN CREATE New Record in Account and Contact objects
                     After new Account, Contact is created - INSERT new Opportunity record
                     
                     2) DealerTrackMonthlyLoans import
                     To assist in loading Monthly Dealer track data from TmpLoadPerformance and insert into Opportunity 
                     This will then trigger a routine to insert new records in Opportunity
                     Look for Previous AWX no
                     If not found THEN CREATE New Record in Account object
                     After new Account INSERT new Opportunity record
                     
      * param     :  TmpLoadPerformance__c records to process
     */
    public static void uploadRecords( List<TmpLoadPerformance__c> newLoadPerformances ) {
        String defaultOwnerName = 'Steve Ouellette';
        
        Id defaultOwnerId = [ SELECT Id FROM User WHERE Name = :defaultOwnerName LIMIT 1 ].id;
        
        String recordTypeId = getOpportunityRecordType('Monthly AutoWorx DT-Route One');
        System.debug('recordTypeId : ' + recordTypeId);
        
        Set<String> autoworxNoSet = getAutoworxNumbers( newLoadPerformances );
        
        Date currentCloseDate = getCurrentCloseDate( newLoadPerformances );
        
        System.debug(' currentCloseDate : ' + currentCloseDate );
        
        Map<String,Account> autoworxNoToAccount;
        Set<String> newAutowoxNumbers = new Set<String>();
        if( autoworxNoSet != null && !autoworxNoSet.isEmpty()) {
            autoworxNoToAccount = getAutoworxNoToAccount(autoworxNoSet);
            System.debug('autoworxNoToAccount : ' + autoworxNoToAccount);
        }
        
        List<TmpLoadPerformance__c> unMatchedloadPerformances = getUnmatchedLoadPerformances(newLoadPerformances, autoworxNoToAccount, recordTypeId);
        Map<Id,TmpLoadPerformance__c> accountIdTOLoadPerformance = new Map<Id,TmpLoadPerformance__c>();
        
        //--If related account not found create opportunities, accounts and contacts
        System.debug('unMatchedloadPerformances : ' + json.serialize(unMatchedloadPerformances));
        
        Database.SaveResult[] accountInsertionResult;
        Map<Id,String> LoadPerformanceIdToErrorMsg = new Map<Id,String>();
        List<Contact> contactsToInsert = new List<Contact>();
        
        
        if( unMatchedloadPerformances != null && !unMatchedloadPerformances.isEmpty() ) {
            
            List<Account> accountsToInsert = getAccountsToInsert(unMatchedloadPerformances, defaultOwnerId);
            
            createAcounts( unMatchedloadPerformances, 
                           accountsToInsert,
                           accountInsertionResult, 
                           LoadPerformanceIdToErrorMsg, 
                           contactsToInsert, 
                           accountIdTOLoadPerformance,
                           newAutowoxNumbers,
                           autoworxNoToAccount,
                           defaultOwnerId );
            
            if( !contactsToInsert.isEmpty() ) {
                createContacts( accountsToInsert, 
                                autoworxNoToAccount, 
                                LoadPerformanceIdToErrorMsg, 
                                accountIdTOLoadPerformance, 
                                contactsToInsert,
                                newAutowoxNumbers );
            }
            System.debug('newAutowoxNumbers : ' + newAutowoxNumbers);
            
        }
        insertOpportunities( newLoadPerformances, 
                             LoadPerformanceIdToErrorMsg, 
                             autoworxNoToAccount, 
                             recordTypeId, 
                             newAutowoxNumbers, 
                             defaultOwnerId,
                             currentCloseDate );
    }
    
    /**
      * @purpose : Insert Account records if TmpLoadPerformance awx number is not found
                   (Used for both Awx import and Monthly Deler track Loan import)
      * @param   : 1) unMatchedloadPerformances - TmpLoadPerformances with new atoworx numbers
                   2) LoadPerformanceIdToErrorMsg - map to maintain error msg corresponding to the LoadPerformance
                   3) accountIdTOLoadPerformance - map to maintain account corresponding to the LoadPerformance
     */
    static void createAcounts( List<TmpLoadPerformance__c> unMatchedloadPerformances,
                               List<Account> accountsToInsert,
                               Database.SaveResult[] accountInsertionResult,
                               Map<Id,String> LoadPerformanceIdToErrorMsg,
                               List<Contact> contactsToInsert,
                               Map<Id,TmpLoadPerformance__c> accountIdTOLoadPerformance,
                               Set<String> newAutowoxNumbers,
                               Map<String,Account> autoworxNoToAccount,
                               String defaultOwnerId
                             ) {
        Integer recordCount = 0;
        
        accountInsertionResult = Database.Insert(accountsToInsert,false);
        System.debug('accountsToInsert  : ' + json.serialize(accountsToInsert));
        if (accountInsertionResult != null){
            for (Database.SaveResult result : accountInsertionResult) {
                if (!result.isSuccess()) {
                    String errorMsg = getErrorMsg(result);
                    LoadPerformanceIdToErrorMsg.put(unMatchedloadPerformances[recordCount].Id,'(Account) ' + errorMsg);
                } else {
                    if( String.isEmpty(unMatchedloadPerformances[recordCount].Tmp_Month_ID__c) ) {
                        if( String.isNotBlank(unMatchedloadPerformances[recordCount].Tmp_Account_Name__c) ) {
                            contactsToInsert.add(buildContact(unMatchedloadPerformances[recordCount], result.getId(), defaultOwnerId));
                            accountIdTOLoadPerformance.put(result.getId(),unMatchedloadPerformances[recordCount]);
                        }
                    }
                    
                    newAutowoxNumbers.add(unMatchedloadPerformances[recordCount].Tmp_Autoworx_no__c);
                    autoworxNoToAccount.put(unMatchedloadPerformances[recordCount].Tmp_Autoworx_no__c,accountsToInsert[recordCount]);
                   
                }
                recordCount++;
            }
        }
    }
    
    /**
      * @purpose : Get Error Message
      * @param   : result - records save result
      * @Return  : String - error msgs string
     */
    static String getErrorMsg( Database.SaveResult result ) {
        String errorMsg = '';
        Database.Error[] errs = result.getErrors();
        for(Database.Error err : errs) {
            System.debug(err.getStatusCode() + ' - ' + err.getMessage());
            errorMsg += err.getMessage() + ';';
        }
        return errorMsg;
    }
    
    /**
      * @purpose : Create contacts if autoworx number is new.
     */
    static void createContacts( List<Account> accountsToInsert, 
                                Map<String,Account> autoworxNoToAccount,
                                Map<Id,String> LoadPerformanceIdToErrorMsg,
                                Map<Id,TmpLoadPerformance__c> accountIdTOLoadPerformance,
                                List<Contact> contactsToInsert,
                                Set<String> newAutowoxNumbers
                              ) {
         if( ! contactsToInsert.isEmpty() ) {
            Integer contactCount = 0;
            List<Account> accountsToDelete = new List<Account>();
            Database.SaveResult[] contactInsertionResult = Database.Insert(contactsToInsert,false);
            System.debug('contactInsertionResult  : ' + json.serialize(contactInsertionResult));
            if (contactInsertionResult != null){
                for (Database.SaveResult result : contactInsertionResult) {
                    if (!result.isSuccess()) {
                        String errorMsg = getErrorMsg(result);
                        accountsToDelete.add(new Account( id = contactsToInsert[contactCount].accountId));
                        System.debug('accountsToDelete : ' + accountsToDelete);
                        LoadPerformanceIdToErrorMsg.put(accountIdTOLoadPerformance.get(contactsToInsert[contactCount].accountId).id,'(Contact) ' + errorMsg);
                        
                        newAutowoxNumbers.remove(accountIdTOLoadPerformance.get(contactsToInsert[contactCount].accountId).Tmp_Autoworx_no__c);
                        autoworxNoToAccount.remove(accountIdTOLoadPerformance.get(contactsToInsert[contactCount].accountId).Tmp_Autoworx_no__c);
                        accountIdTOLoadPerformance.remove(contactsToInsert[contactCount].accountId);
                        
                    } else{
                        System.debug('accountsToInsert[contactCount].AutoWorx_no__c ' + accountsToInsert[contactCount].AutoWorx_no__c);
                        
                        System.debug('autoworxNoToAccount '+ contactCount + ' ' + accountsToInsert[contactCount].AutoWorx_no__c);
                    }
                    contactCount++;
                }
            }
            
            if( ! accountsToDelete.isEmpty() ) {
            
                Database.delete(accountsToDelete,false);
                System.debug('accountsToDelete after del : ' + accountsToDelete);
            }
        }
    }
    
    /**
      * @purpose : Get autoworx number from list of new LoadPerformances
      * @Return  : Set of autoworx numbers
     */
    static Set<String> getAutoworxNumbers( List<TmpLoadPerformance__c> newLoadPerformances ) {
        Set<String> autoworxNoSet = new Set<String>();
        
        for( TmpLoadPerformance__c loadPerformance : newLoadPerformances ) {
            if( String.isNotBlank(loadPerformance.Tmp_Autoworx_no__c)) {
                autoworxNoSet.add(loadPerformance.Tmp_Autoworx_no__c);
            }
        }
        
        return autoworxNoSet;
    }
    
    
    /**
      * @purpose : Get autoworx number from list of new LoadPerformances
      * @Return  : Set of autoworx numbers
     */
    static Date getCurrentCloseDate( List<TmpLoadPerformance__c> newLoadPerformances ) {
        Date currentCloseDate;
        for( TmpLoadPerformance__c loadPerformance : newLoadPerformances ) {
            if( loadPerformance.Tmp_Close_Date__c != null ) {
                currentCloseDate = loadPerformance.Tmp_Close_Date__c;
                break;
            }
        }
        return currentCloseDate;
    }
    
    /**
      * @purpose : Get autoworx number from list of new LoadPerformances
      * @Return  : Get List of new load performances to create accounts/contacts
     */
    static List<TmpLoadPerformance__c> getUnmatchedLoadPerformances( List<TmpLoadPerformance__c> newLoadPerformances, 
                                                                     Map<String, Account> autoworxNoToAccount,
                                                                     String recordTypeId ) {
        List<TmpLoadPerformance__c> unMatchedloadPerformances = new List<TmpLoadPerformance__c>();
        
        for( TmpLoadPerformance__c loadPerformance : newLoadPerformances ) {
            if( String.isNotBlank(recordTypeId) && String.isNotBlank(loadPerformance.Tmp_Opportunity_Record_Type__c) ) {
                recordTypeId = get15DigitRecordTypeId(loadPerformance.Tmp_Opportunity_Record_Type__c, recordTypeId );
            }
            if( loadPerformance.Tmp_Opportunity_Record_Type__c == recordTypeId 
                && String.isNotEmpty( loadPerformance.Tmp_Autoworx_no__c ) ) {
                if( autoworxNoToAccount != null  ) {
                    if(!autoworxNoToAccount.containsKey(loadPerformance.Tmp_Autoworx_no__c) ) {
                        unMatchedloadPerformances.add(loadPerformance);
                    }
                } else {
                    unMatchedloadPerformances.add(loadPerformance);
                }
            } else if( String.isNotBlank(loadPerformance.Tmp_Month_ID__c)
                       && String.isNotEmpty( loadPerformance.Tmp_Autoworx_no__c ) ) {
                if( autoworxNoToAccount != null  ) {
                    if(!autoworxNoToAccount.containsKey(loadPerformance.Tmp_Autoworx_no__c) ) {
                        unMatchedloadPerformances.add(loadPerformance);
                    }
                } else {
                    unMatchedloadPerformances.add(loadPerformance);
                }   
            }
        }
        
        return unMatchedloadPerformances;
    }
    
    /**
      * @purpose : Get 15 digit record type id from 18 digit record type id
      * @Return  : 15 digit record type id
     */
    static String get15DigitRecordTypeId( String tmpRecordTypeId, string oppoRecordTypeId ) {
        
        if(String.isNotBlank(tmpRecordTypeId) && (String.isNotBlank(oppoRecordTypeId))) {
            if( tmpRecordTypeId.length() == 15 ) {
                oppoRecordTypeId = oppoRecordTypeId.substring(0, 15);
            }
        }
        return oppoRecordTypeId;
    }
    
    /**
      * @purpose : Insert opportunities with new mapping depends upon the record type 
                   i.e. autoworx import/ Monthly dealer track loan import
     */
    static void insertOpportunities( List<TmpLoadPerformance__c> newLoadPerformances,
                                     Map<Id,String> LoadPerformanceIdToErrorMsg,
                                     Map<String, Account> autoworxNoToAccount,
                                     String recordTypeId ,
                                     Set<String> newAutowoxNumbers,
                                     String defaultOwnerId,
                                     Date currentCloseDate ) {
        List<Opportunity> opportunitiesToInsert = new List<Opportunity>();
        List<TmpLoadPerformance__c> loadPerformancesRecords = new List<TmpLoadPerformance__c>();
        System.debug('LoadPerformanceIdToErrorMsg in insert : ' + LoadPerformanceIdToErrorMsg);
        for( TmpLoadPerformance__c loadPerformance : newLoadPerformances ) {
             
            if( LoadPerformanceIdToErrorMsg.containsKey(loadPerformance.id)) {
                System.debug('loadPerformance in insert : ' + loadPerformance);
                loadPerformance.addError( 'Error occurred for ' + 
                                          loadPerformance.Tmp_Autoworx_no__c + ' : ' + 
                                          LoadPerformanceIdToErrorMsg.get(loadPerformance.id));
            } else {
                if( ( loadPerformance.Tmp_Opportunity_Record_Type__c == 
                      get15DigitRecordTypeId(loadPerformance.Tmp_Opportunity_Record_Type__c, recordTypeId)
                    || String.isNotBlank(loadPerformance.Tmp_Month_ID__c))
                    && String.isNotBlank(loadPerformance.Tmp_Autoworx_no__c ) 
                    && autoworxNoToAccount.containsKey(loadPerformance.Tmp_Autoworx_no__c)) {
                    if( newAutowoxNumbers.contains(loadPerformance.Tmp_Autoworx_no__c ) ) {
                        opportunitiesToInsert.add(buildOpportunity( autoworxNoToAccount.get(loadPerformance.Tmp_Autoworx_no__c), 
                                                                    loadPerformance, 
                                                                    defaultOwnerId,
                                                                    currentCloseDate
                                                                   ));
                    } else {
                        opportunitiesToInsert.add(buildOpportunity( autoworxNoToAccount.get(loadPerformance.Tmp_Autoworx_no__c), 
                                                                    loadPerformance,
                                                                    autoworxNoToAccount.get(loadPerformance.Tmp_Autoworx_no__c).ownerId,
                                                                    currentCloseDate ));
                    }
                } else {
                    opportunitiesToInsert.add(buildOpportunity(new Account(), loadPerformance, defaultOwnerId, currentCloseDate));
                }
                loadPerformancesRecords.add(loadPerformance);
            }
        }
        
        if( opportunitiesToInsert != null && !opportunitiesToInsert.isEmpty()) {
            Database.SaveResult[] results = Database.Insert(opportunitiesToInsert,false);
            System.debug('opportunitiesToInsert aftr  : ' + opportunitiesToInsert);
            if (results != null) {
                addErrorMsgs(results, loadPerformancesRecords);
            }
        }
        
        System.debug('opportunitiesToInsert : ' + json.serialize(opportunitiesToInsert));
    }
    
    /**
      * @purpose : Build map of autoworxNumber to its account
      * @return  : Map of awx number to account
     */
    static Map<String, Account> getAutoworxNoToAccount( Set<String> autoworxNoSet ) {
        
        Map<String,Account> autoworxNoToAccount = new Map<String,Account>();
        
        //--Find related accounts with autoworx number and relate them to the corresponding opportunity
        if( autoworxNoSet != null && !autoworxNoSet.isEmpty() ) {
            for( Account account : [ SELECT Id, Name, AutoWorx_no__c, OwnerId, Autoworx_prev_no__c 
                                     FROM Account
                                     WHERE AutoWorx_no__c IN : autoworxNoSet OR
                                           Autoworx_prev_no__c IN : autoworxNoSet ] ) {
                
                if( String.isNotBlank(account.AutoWorx_no__c) ) {
                    autoworxNoToAccount.put(account.AutoWorx_no__c,account);
                }
                if( String.isNotBlank(account.Autoworx_prev_no__c) && !autoworxNoToAccount.containsKey(account.Autoworx_prev_no__c) ) {
                    System.debug('Autoworx_prev_no__c : ' + account.Autoworx_prev_no__c);
                    autoworxNoToAccount.put(account.Autoworx_prev_no__c,account);   
                }
            }
        }
        
        return autoworxNoToAccount;
    }
    
     /**
       * @purpose : Get opportunity record type
       * @return  : String - record type id 
      */
    static String getOpportunityRecordType(String recordTypeName) {
       /* return ( [ SELECT Id, SobjectType, DeveloperName 
                   FROM RecordType 
                   WHERE DeveloperName = :developerName AND SobjectType = 'Opportunity'] ).id;*/
        return Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    
    /**
      * @purpose : add error messages
     */
    static void addErrorMsgs(Database.SaveResult[] results, List<TmpLoadPerformance__c> newLoadPerformances) {
        Integer loadPerformanceCount = 0;
        if (results != null){
            for (Database.SaveResult result : results) {
                if (!result.isSuccess()) {
                    String errorMsg = getErrorMsg(result);
                    System.debug('loadPerformance in error : ' + newLoadPerformances[loadPerformanceCount]);
                    System.debug('loadPerformance in error msg : ' + errorMsg );
                    newLoadPerformances[loadPerformanceCount].addError( 'Error occurred for ' + 
                                                                        newLoadPerformances[loadPerformanceCount].Tmp_Autoworx_no__c + ' : ' + 
                                                                        '(Opportunity)' + errorMsg );
                    
                }
                loadPerformanceCount++;
            }
        }
    }
    
    /**
      * @purpose : Build account records with new mapping depends upon the record type 
                   i.e. autoworx import/ Monthly dealer track loan import
      * @return  : List of new dealers to insert
     */
    static List<Account> getAccountsToInsert(List<TmpLoadPerformance__c> unMatchedloadPerformances, String defaultOwnerId ) {
        List<Account> accountsToInsert = new List<Account>();
        Account accountToInsert;
        
        Map<String, Id> mapCityNameToCityId = new Map<String, Id>();
        for( TmpLoadPerformance__c loadPerformance : unMatchedloadPerformances ) {
            if( String.isNotBlank(loadPerformance.Tmp_Shipping_city__c) ) {
                mapCityNameToCityId.put( loadPerformance.Tmp_Shipping_city__c, null);
            }
        }
        
        for( City__c city : [ SELECT Id, Name
                              FROM City__c 
                              WHERE Name IN : mapCityNameToCityId.keySet() ] ) {
            mapCityNameToCityId.put( city.Name, city.Id );
        }
        
        for( TmpLoadPerformance__c loadPerformance : unMatchedloadPerformances ) {
            
            accountToInsert = new Account();
            accountToInsert.AutoWorx_no__c = loadPerformance.Tmp_Autoworx_no__c;
            if( String.isEmpty(loadPerformance.Tmp_Month_ID__c) ) {
                
                accountToInsert.Phone = loadPerformance.Tmp_Phone__c;
                accountToInsert.Dealer_language__c = loadPerformance.Tmp_Dealer_language__c;
                accountToInsert.ShippingStreet = loadPerformance.Tmp_Shipping_street__c;
                accountToInsert.ShippingCity = loadPerformance.Tmp_Shipping_city__c;
                accountToInsert.ShippingPostalCode = loadPerformance.Tmp_Shipping_Zip_Postal_code__c;
                
                accountToInsert.isUpload__c = true;
                accountToInsert.Hogan_no__c = loadPerformance.Tmp_Hogan_no__c;
                accountToInsert.Account_Legal_Name__c  = loadPerformance.Tmp_Legal_Name__c;
                accountToInsert.ShippingCountry  = 'CANADA';
                accountToInsert.Province_Code__c = loadPerformance.Tmp_Shipping_province__c;
            } else {
                accountToInsert.DealerTrack_no__c = loadPerformance.Tmp_DealerTrack_no__c;
            }
            if( String.isNotBlank(defaultOwnerId) ) {
                accountToInsert.ownerId = defaultOwnerId;
            }
            if( String.isNotBlank(loadPerformance.Tmp_Account_Name__c) ) {
                accountToInsert.Name = loadPerformance.Tmp_Account_Name__c;
            } else {
                accountToInsert.Name = 'Nouveau Marchand AWX#';
                if( String.isNotBlank(loadPerformance.Tmp_Autoworx_no__c) ) {
                    accountToInsert.Name += loadPerformance.Tmp_Autoworx_no__c;
                }
            }
            if( mapCityNameToCityId.containsKey(loadPerformance.Tmp_Shipping_city__c) 
                && mapCityNameToCityId.get(loadPerformance.Tmp_Shipping_city__c) != null ) {
                accountToInsert.City1__c = mapCityNameToCityId.get( loadPerformance.Tmp_Shipping_city__c );
            }
            accountsToInsert.add(accountToInsert);
        }
        
        return accountsToInsert;
    }
    
    /**
      * @purpose : Build contact record - it's only for awx import=
      * @return  : Contact record with new details
     */
    static Contact buildContact( TmpLoadPerformance__c unMatchedloadPerformance,
                                  String accountId, String defaultOwnerId ) {
        Contact contactToInsert;
        contactToInsert = new Contact();
        contactToInsert.accountId = accountId;
        
        if( String.isNotBlank(unMatchedloadPerformance.Tmp_Contact_last_name__c) ) {
            List<String> name = unMatchedloadPerformance.Tmp_Contact_last_name__c.split(' ');
            if( name != null && !name.isEmpty() ) {
                contactToInsert.FirstName = name[0];
                if( name.size() > 1 ) {
                    contactToInsert.LastName = name[1];    
                }
            } else {
                contactToInsert.LastName = unMatchedloadPerformance.Tmp_Contact_last_name__c;    
            }
        }
        contactToInsert.Email = 'email@gmail.com';
        contactToInsert.Phone = unMatchedloadPerformance.Tmp_Phone__c;
        contactToInsert.isUpload__c = true;
        contactToInsert.MailingStreet = unMatchedloadPerformance.Tmp_Shipping_street__c;
        contactToInsert.MailingCity = unMatchedloadPerformance.Tmp_Shipping_city__c;
        contactToInsert.Province_Code__c = unMatchedloadPerformance.Tmp_Shipping_province__c;
        contactToInsert.MailingPostalCode = unMatchedloadPerformance.Tmp_Shipping_Zip_Postal_code__c;
        contactToInsert.MailingCountry = 'CANADA';
        
        if( String.isNotBlank(defaultOwnerId) ) {
            contactToInsert.ownerId = defaultOwnerId;
        }
        return contactToInsert;
    }
    
    /**
      * @purpose : Build opportunity record with new mapping depends upon the record type 
                   i.e. autoworx import/ Monthly dealer track loan import
      * @Return  : Opportunity record with new field values
     */
    static Opportunity buildOpportunity( Account account, 
                                         TmpLoadPerformance__c newLoadPerformance, 
                                         String oppOwnerId, 
                                         Date currentCloseDate ) {
        Opportunity opportunity = new Opportunity();
        if(String.isNotBlank(oppOwnerId)) {
            opportunity.ownerId = oppOwnerId;
        }
        
        if( String.isNotBlank( newLoadPerformance.Tmp_Month_ID__c ) ) {
            //--Calculate closedate from MonthId eg. 201604 then date will be 30 April 2016
            Integer year = Integer.valueOf(newLoadPerformance.Tmp_Month_ID__c.substring(0, 4));
            Integer month = Integer.valueOf(newLoadPerformance.Tmp_Month_ID__c.substring(4));
            Integer numberDays = date.daysInMonth(year,month);
            Date closeDate = date.newinstance(year, month, numberDays);
                                
            if(account != null && account.id != null ) {
                opportunity.accountId = account.id;
                if(String.isNotBlank(account.Name)) {
                    opportunity.Name = getOpportunityName(closeDate,account.Name + ' Perf. DT ' );
                }     
            }
            opportunity.closeDate = closeDate;
            opportunity.StageName = 'Book DT';
            opportunity.Probability = 0;
            opportunity.type = 'DT';
            
            opportunity.First_Submits__c = newLoadPerformance.Tmp_First_Submits__c;
            opportunity.DESJ_approved_all__c = newLoadPerformance.Tmp_DESJ_approved_all__c;
            opportunity.DESJ_declined_all__c = newLoadPerformance.Tmp_DESJ_declined_all__c;
            opportunity.DESJ_Look_Non_subvented__c = newLoadPerformance.Tmp_DESJ_Look_Non_subvented__c;
            opportunity.Dealer_Book_Prime_Non_subvented__c = newLoadPerformance.Tmp_Dealer_Book_Prime_Non_subvented__c;
            opportunity.DESJ_Approved_non_subvented__c = newLoadPerformance.Tmp_DESJ_Approved_non_subvented__c;
            opportunity.DESJ_Declined_non_subvented__c = newLoadPerformance.Tmp_DESJ_Declined_non_subvented__c;
            opportunity.Avg_Term__c = newLoadPerformance.Tmp_Avg_Term__c;
            opportunity.Asset_Type_FABD__c = newLoadPerformance.Tmp_Asset_Type__c;
            opportunity.DESJ_Book_Non_subvented__c = newLoadPerformance.Tmp_DESJ_Book_Non_subvented__c;
            opportunity.Dealer_Approved_Prime_Non_subvented__c = newLoadPerformance.Tmp_Dealer_Approved_Prime_Non_subvented__c;
            opportunity.Dealer_Declined_Prime_Non_subvented__c = newLoadPerformance.Tmp_Dealer_Declined_Prime_Non_subvented__c;
            opportunity.Dealer_Look_Prime_Non_subvented__c = newLoadPerformance.Tmp_Dealer_Look_Prime_Non_subvented__c;
            
            String recordTypeId = getOpportunityRecordType('DealerTrackMonthlyLoans');
            if( String.isNotBlank(recordTypeId)) {
                opportunity.recordTypeId = recordTypeId;
            }
        } else {
            if(account != null && account.id != null ) {
                opportunity.accountId = account.id;
                if(String.isNotBlank(account.Name)) {
                    opportunity.Name = getOpportunityName(newLoadPerformance.Tmp_Close_Date__c, account.Name + ' Perf. AWX DT ');
                }     
            }
            if( newLoadPerformance.Tmp_Close_Date__c == null ) {
                opportunity.closeDate = currentCloseDate;
            } else {
                opportunity.closeDate = newLoadPerformance.Tmp_Close_Date__c;
            }
            opportunity.StageName = newLoadPerformance.Tmp_Stage__c;
            opportunity.Probability = newLoadPerformance.Tmp_Probability__c;
            opportunity.type = newLoadPerformance.Tmp_Type__c;
            
            if( String.isNotBlank(newLoadPerformance.Tmp_Opportunity_Record_Type__c) ) {
                opportunity.recordTypeId = newLoadPerformance.Tmp_Opportunity_Record_Type__c;
            }
        }
        
        if( String.isBlank(opportunity.Name)) {
            opportunity.Name = newLoadPerformance.Tmp_Opportunity_Name__c; 
        }
        opportunity.amount = newLoadPerformance.Tmp_Amount__c;
        opportunity.Autoworx_Performance_no__c = newLoadPerformance.Tmp_Autoworx_no__c;
        opportunity.DESJ_approved_all__c = newLoadPerformance.Tmp_DESJ_approved_all__c;
        opportunity.DESJ_book_all__c = newLoadPerformance.Tmp_DESJ_book_all__c;
        opportunity.DESJ_Look_all__c = newLoadPerformance.Tmp_DESJ_Look_all__c;
        
        return opportunity;
    }
    
    /**
      * @purpose : get the opportunity name in specific format
      * @return  : well formated account name
     */
    static String getOpportunityName( Date closeDate, String accountName ) {
        
        if( closeDate != null ) {
            accountName += closeDate.year() + '-';
            accountName += closeDate.month(); 
        }
        return accountName;
    }
    
    /**
      * @purpose : Delete TmpLoadPerformance__c records after processing
      * @param   : 1) TmpLoadPerformance__c records
     */
    public static void deleteLoadPerformances(List<TmpLoadPerformance__c> newLoadPerformances ) {
        Database.DeleteResult[] results = Database.delete(newLoadPerformances,false);
        Integer loadPerformanceCount = 0;
        if (results != null) {
            for (Database.DeleteResult result : results) {
                if (!result.isSuccess()) {
                    Database.Error[] errs = result.getErrors();
                    String errorMsg = '';
                    for(Database.Error err : errs) {
                        System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                        errorMsg += err.getMessage() + ';';
                        newLoadPerformances[loadPerformanceCount].addError(errorMsg);
                    }
                }
                loadPerformanceCount++;
            }
        }
    }
}