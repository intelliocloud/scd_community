/***********************************************************************************************************************************
@purpose: Dashboard002Controller Test
@Created Date: 06 - Aug- 2016
************************************************************************************************************************************/
@isTest
private Class TestDashboard002Controller {

    /**
     *@purpose: To create Test data.
    **/
    @testSetup
    private static void createTestDataForDealerReport(){
        Account account = new Account(Name = 'Test Account', AutoWorx_no__c = '001', AnnualRevenue = 100000);
        Insert account;
        List<Opportunity> OpptyList = createOpportunities(100, 'Monthly AutoWorx DT-Route One', account);
        INSERT OpptyList;
    }
    
    /**
     *@purpose: To create test opportunities
     *@param: a)totalopptyTocreate b)recordTypeName c)account
     *@return: List of Opportunities
    **/   
    private static List<Opportunity> createOpportunities (Integer totalopptyTocreate, String recordTypeName, Account account){
        
        List<Opportunity> OpptyList = new List<Opportunity>();
        Map<String, Id> recordtypeNameToIdMap = buildRecordtypeNameToIdMap('Opportunity');
        Date todaysDate = System.today();
        
        for(Integer i=0; i<totalopptyTocreate; i++){
            
            OpptyList.add(new Opportunity(Name = 'TestOpportunity'+i, DESJ_Look_all__c = i, DESJ_approved_all__c = i,
                                          DESJ_book_all__c = i, Avg_Total_Amount_Financed__c = i, Amount = i,
                                          RecordTypeId = recordtypeNameToIdMap.get(recordTypeName),
                                          AccountId = account.Id, StageName = 'Book', CloseDate = todaysDate));
                                          
            OpptyList.add(new Opportunity(Name = 'TestOpportunity'+i, DESJ_Look_all__c = i+1, DESJ_approved_all__c = i+1,
                                          DESJ_book_all__c = i+1, Avg_Total_Amount_Financed__c = i+1, Amount = i+1,
                                          RecordTypeId = recordtypeNameToIdMap.get(recordTypeName),
                                          AccountId = account.Id, StageName = 'Book', CloseDate = todaysDate.addYears(-1)));
        }
        
        return OpptyList;
    }
    
    private static Map<String, Id> buildRecordtypeNameToIdMap(String SobjectName){
        
        Map<String, Id> recordtypeNameToIdMap = new Map<String, Id>();
        
        for(RecordType recType :[SELECT Id, Name FROM RecordType WHERE SobjectType =: SobjectName]){
            recordtypeNameToIdMap.put(recType.Name, recType.Id);
        }
        
        return recordtypeNameToIdMap;
    }
    
    /**
     *@purpose:generateReport Test
    **/
    static testmethod void generateReportTest(){
        
        Account account = [SELECT Id, AnnualRevenue, AutoWorx_no__c, Name FROM Account LIMIT 1];
        Date todaysDate = System.today();
        Dashboard002Controller.DeakerReportWrapper dealerReportData = new Dashboard002Controller.DeakerReportWrapper();
        
        dealerReportData = Dashboard002Controller.generateReport(todaysDate.year(), todaysDate.month(), account.Id);
        
        System.assertEquals(account.AnnualRevenue, dealerReportData.objCurrentYear);
        System.assertEquals(account.AutoWorx_no__c, dealerReportData.autoWorxNoStr);
        summarizeDataAssert(4950, dealerReportData.currentYearSummarizeData);
        summarizeDataAssert(5050, dealerReportData.prevYearSummarizeData);
    }
    
    /**
     *@purpose: Opportunity assert
    **/
    private static void summarizeDataAssert(Integer summarizeValue, 
                                            Dashboard002Controller.summarizeOpportunityWrapper summarizeOpportunityWrapper){
        
        System.assertEquals(summarizeValue, summarizeOpportunityWrapper.look);
        System.assertEquals(summarizeValue,  summarizeOpportunityWrapper.approval);
        System.assertEquals(summarizeValue,  summarizeOpportunityWrapper.book);
        System.assertEquals(1,  summarizeOpportunityWrapper.averageLoan); 
        System.assertEquals(summarizeValue,  summarizeOpportunityWrapper.totalVolume); 
    }
}