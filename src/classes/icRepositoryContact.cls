public class icRepositoryContact implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		Contact getContactInfoById(String contactId);
		List<Contact> getContactsByDealerNames(Set<String> dealerNames);
	}

	public class Impl implements IClass {

		public Contact getContactInfoById(String contactId) {
			return [SELECT Id, FirstName, LastName, AccountId, Account.OwnerId FROM Contact WHERE Id = :contactId];
		}
		
		public List<Contact> getContactsByDealerNames(Set<String> dealerNames) {
			return [SELECT Id, FirstName, LastName, Title, Account_Contact_Role__c, Account.Name FROM Contact WHERE Account.Name IN :dealerNames];
		}		
	}
}