@isTest
public class icBusinessLogicAccountMock implements icBusinessLogicAccount.IClass{

	public Account getAccountByName(String accountName) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAccountByName');
		params.put('accountName', accountName);
		Return (Account) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAccountByName');
	}

	public List<Account> getPartnerAccountsFromList(Set<String> accountIds) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPartnerAccountsFromList');
		params.put('accountIds', accountIds);
		Return (List<Account>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPartnerAccountsFromList');
	}

	public Map<String, Account> getMapAccountById(List<Account> accounts) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMapAccountById');
		params.put('accounts', accounts);
		Return (Map<String, Account>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMapAccountById');
	}
}