global with sharing class icCTRLContactUsInfo {
	static icBusinessLogicUser.IClass logicUser = (icBusinessLogicUser.IClass) icObjectFactory.getSingletonInstance('icBusinessLogicUser');
	static icBusinessLogicContact.IClass logicContact = (icBusinessLogicContact.IClass) icObjectFactory.getSingletonInstance('icBusinessLogicContact');

	@AuraEnabled
	global static icDTOContactUsInfo getContactUsInfo() {
		User thisUserInfo = logicUser.getUserInfoById(UserInfo.getUserId());
		Contact thisUserContactInfo = logicContact.getContactInfoById(thisUserInfo.ContactId);
		User salesRepInfo = logicUser.getUserInfoById(thisUserContactInfo.Account.OwnerId);

		icDTOContactUsInfo contactUsInfo = new icDTOContactUsInfo();
		contactUsInfo.firstName = salesRepInfo.FirstName;
		contactUsInfo.lastName = salesRepInfo.LastName;
		contactUsInfo.title = salesRepInfo.Title;
		contactUsInfo.email = salesRepInfo.Text_Email__c;
		contactUsInfo.phone = salesRepInfo.Phone;
		contactUsInfo.cell = salesRepInfo.MobilePhone;
		contactUsInfo.image = salesRepInfo.FullPhotoUrl; // SmallPhotoUrl

		return contactUsInfo;
	}
}