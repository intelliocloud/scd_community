@isTest
public class icTriggerHandlerContactMock implements icTriggerHandlerContact.IClass{

	public void onBeforeInsert(List<Contact> newContacts, Map<Id, Contact> contactMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeInsert');
		params.put('newContacts', newContacts);
		params.put('contactMap', contactMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeInsert');
	}

	public void onAfterInsert(List<Contact> oldContact, List<Contact> newContacts, Map<Id, Contact> contactMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterInsert');
		params.put('oldContact', oldContact);
		params.put('newContacts', newContacts);
		params.put('contactMap', contactMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterInsert');
	}

	public void onBeforeUpdate(List<Contact> oldContact, List<Contact> newContacts, Map<Id, Contact> contactMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
		params.put('oldContact', oldContact);
		params.put('newContacts', newContacts);
		params.put('contactMap', contactMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeUpdate');
	}

	public void onAfterUpdate(List<Contact> oldContact, List<Contact> newContacts, Map<Id, Contact> contactMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterUpdate');
		params.put('oldContact', oldContact);
		params.put('newContacts', newContacts);
		params.put('contactMap', contactMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterUpdate');
	}

	public void onBeforeDelete(List<Contact> oldContact, Map<Id, Contact> contactMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeDelete');
		params.put('oldContact', oldContact);		
		params.put('contactMap', contactMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onBeforeDelete');
	}

	public void onAfterDelete(List<Contact> oldContact, Map<Id, Contact> contactMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterDelete');
		params.put('oldContact', oldContact);		
		params.put('contactMap', contactMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterDelete');
	}

	public void onAfterUndelete(List<Contact> oldContact, List<Contact> newContacts, Map<Id, Contact> contactMap) {
		Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterUndelete');
		params.put('oldContact', oldContact);
		params.put('newContacts', newContacts);
		params.put('contactMap', contactMap);
		//Return (typecase) icTestMockUtilities.Tracer.GetReturnValue(this, 'onAfterUndelete');
	}
}